import java.io.IOException;

import jdk.dio.DeviceManager;
import jdk.dio.gpio.GPIOPin;
import jdk.dio.gpio.GPIOPinConfig;

public class Out {

	public static void main(String[] args) throws IOException  {
		// TODO Auto-generated method stub

		GPIOPin pin = null;
		try {
			int pinNumber = Integer.parseInt(args[0]);
			System.out.println("try to connect to pin " + pinNumber + " as output GPIO");
    		GPIOPinConfig pinConfig = new GPIOPinConfig(0, pinNumber, GPIOPinConfig.DIR_OUTPUT_ONLY, GPIOPinConfig.DEFAULT, GPIOPinConfig.TRIGGER_BOTH_EDGES, false);
			pin = DeviceManager.open(GPIOPin.class, pinConfig);
			
			boolean value = true;
			while(true) {
				pin.setValue(value);
				value = !value;
				Thread.sleep(1000);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		} finally {
			pin.close();
		}
	}

}
