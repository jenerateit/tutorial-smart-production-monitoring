import java.io.IOException;

import jdk.dio.DeviceManager;
import jdk.dio.gpio.GPIOPin;
import jdk.dio.gpio.GPIOPinConfig;
import jdk.dio.gpio.PinEvent;
import jdk.dio.gpio.PinListener;

public class In {

	public static void main(String[] args) throws IOException  {
		// TODO Auto-generated method stub

		GPIOPin pin = null;
		try {
			int pinNumber = Integer.parseInt(args[0]);
			System.out.println("try to connect to pin " + pinNumber + " as input GPIO");
    		GPIOPinConfig pinConfig = new GPIOPinConfig(0, pinNumber, GPIOPinConfig.DIR_INPUT_ONLY, GPIOPinConfig.DEFAULT, GPIOPinConfig.TRIGGER_BOTH_EDGES, false);
			pin = DeviceManager.open(GPIOPin.class, pinConfig);
			
			pin.setInputListener(new PinListener() {
				
				@Override
				public void valueChanged(PinEvent event) {
					System.out.println("value: " + event.getValue());
				}
			});
			
			while(true) {
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		} finally {
			pin.close();
		}
	}

}
