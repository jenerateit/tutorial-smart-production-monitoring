package org.gs.iot.sample.iot.driver.lib;

import java.io.IOException;
import java.nio.ByteBuffer;

import jdk.dio.ClosedDeviceException;
import jdk.dio.UnavailableDeviceException;
import jdk.dio.i2cbus.I2CDevice;

public final class I2CDeviceUtil {

	public static final int I2C_TIMEOUT = 5000;
	public static final int I2C_CLOCK_FREQUENCY = 1000;

	private I2CDeviceUtil() {
		throw new AssertionError();
	}

	public static void writeTransactional(I2CDevice i2cDevice, int address, int data)
			throws UnavailableDeviceException, ClosedDeviceException, IOException {
		try {
			i2cDevice.begin();
			i2cDevice.write(address);
			i2cDevice.write(data);
			i2cDevice.end();
		}
		catch(IllegalStateException | IllegalArgumentException e) { // IllegalArgumentException is thrown if dev is not
																	// available, dio bug?
			throw new IOException("error writing to i2c device", e);
		}
	}

	public static void write(I2CDevice i2cDevice, int address, int data)
			throws UnavailableDeviceException, ClosedDeviceException, IOException {
		try {
			i2cDevice.write(address, 1, ByteBuffer.wrap(new byte[] { (byte) data }));
		}
		catch(IllegalStateException | IllegalArgumentException e) { // IllegalArgumentException is thrown if dev is not
																	// available, dio bug?
			throw new IOException("error writing to i2c device", e);
		}
	}

	public static void write(I2CDevice i2cDevice, int address, byte[] buffer, int offset, int size)
			throws UnavailableDeviceException, ClosedDeviceException, IOException {
		try {
			ByteBuffer src = ByteBuffer.wrap(buffer, offset, size);
			i2cDevice.write(address, 1, src);
		}
		catch(IllegalStateException | IllegalArgumentException e) { // IllegalArgumentException is thrown if dev is not
																	// available, dio bug?
			throw new IOException("error writing to i2c device", e);
		}
	}

	public static int read(I2CDevice i2cDevice, int address)
			throws UnavailableDeviceException, ClosedDeviceException, IOException {
		try {
			i2cDevice.begin();
			i2cDevice.write(address);
			i2cDevice.end();
			return i2cDevice.read();
		}
		catch(IllegalStateException | IllegalArgumentException e) {
			throw new IOException("error read from i2c device", e);
		}
	}

	public static void read(I2CDevice i2cDevice, int address, byte[] buffer, int offset, int size)
			throws UnavailableDeviceException, ClosedDeviceException, IOException {
		try {
			ByteBuffer dst = ByteBuffer.wrap(buffer, offset, size);
			i2cDevice.write(address);
			i2cDevice.read(dst);
		}
		catch(IllegalStateException | IllegalArgumentException e) { // IllegalArgumentException is thrown if dev is not
																	// available, dio bug?
			throw new IOException("error writing to i2c device", e);
		}
	}
	
	public static void read(I2CDevice i2cDevice, byte[] buffer, int offset, int size)
			throws UnavailableDeviceException, ClosedDeviceException, IOException {
		try {
			ByteBuffer dst = ByteBuffer.wrap(buffer, offset, size);
			i2cDevice.read(dst);
		}
		catch(IllegalStateException | IllegalArgumentException e) { // IllegalArgumentException is thrown if dev is not
																	// available, dio bug?
			throw new IOException("error writing to i2c device", e);
		}
	}
}
