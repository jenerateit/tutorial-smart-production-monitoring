package org.gs.iot.sample.monitoring.gateway.driverimpl;

import org.slf4j.Logger;
import org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData;
import java.io.IOException;


public class AbstractGatewayAppDeviceDriver { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(AbstractGatewayAppDeviceDriver.class);
    
    
    /**
     *
     * @return
     */
    public AbstractGatewayAppDeviceData getData() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driverimpl.AbstractGatewayAppDeviceDriver.getData.AbstractGatewayAppDeviceData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driverimpl.AbstractGatewayAppDeviceDriver.getData.AbstractGatewayAppDeviceData:DA-ELSE
        return null;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driverimpl.AbstractGatewayAppDeviceDriver.getData.AbstractGatewayAppDeviceData:DA-END
    }
    
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driverimpl.AbstractGatewayAppDeviceDriver.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driverimpl.AbstractGatewayAppDeviceDriver.activate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driverimpl.AbstractGatewayAppDeviceDriver.activate:DA-END
    }
    
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driverimpl.AbstractGatewayAppDeviceDriver.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driverimpl.AbstractGatewayAppDeviceDriver.deactivate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driverimpl.AbstractGatewayAppDeviceDriver.deactivate:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.driverimpl.AbstractGatewayAppDeviceDriver.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driverimpl.AbstractGatewayAppDeviceDriver.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.driverimpl.AbstractGatewayAppDeviceDriver.additional.elements.in.type:DA-END
} // end of java type