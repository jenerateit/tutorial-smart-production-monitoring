package org.gs.iot.sample.monitoring.gateway.driver;

import java.io.IOException;

import org.gs.iot.sample.iot.driver.lib.MCP3008;
import org.gs.iot.sample.monitoring.gateway.devicedata.AnalogConnection;
import org.gs.iot.sample.monitoring.gateway.devicedata.AnalogFaultException;
import org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveData;
import org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveStatusType;
import org.gs.iot.sample.monitoring.gateway.driverimpl.AbstractGatewayAppDeviceDriver;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppInterruptCallback;
import org.slf4j.Logger;


public class SoundSensorGroveDeviceDriver extends AbstractGatewayAppDeviceDriver { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(SoundSensorGroveDeviceDriver.class);
    
    private SoundSensorGroveDeviceDriver.DeviceSetup setup;
    
    private GatewayAppInterruptCallback<SoundSensorGroveData> interruptCallback;
    
    /**
     * creates an instance of SoundSensorGroveDeviceDriver
     *
     * @param setup  the setup
     */
    public SoundSensorGroveDeviceDriver(SoundSensorGroveDeviceDriver.DeviceSetup setup) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.DeviceSetup:DA-ELSE
        super();
        this.setup = setup;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.DeviceSetup:DA-END
    }
    
    
    /**
     * getter for the field setup
     *
     *
     *
     * @return
     */
    public SoundSensorGroveDeviceDriver.DeviceSetup getSetup() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.getSetup.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.getSetup.DeviceSetup:DA-ELSE
        return this.setup;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.getSetup.DeviceSetup:DA-END
    }
    
    /**
     * setter for the field interruptCallback
     *
     *
     *
     * @param interruptCallback  the interruptCallback
     */
    public void setInterruptCallback(GatewayAppInterruptCallback<SoundSensorGroveData> interruptCallback) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.setInterruptCallback.GatewayAppInterruptCallback:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.setInterruptCallback.GatewayAppInterruptCallback:DA-ELSE
        this.interruptCallback = interruptCallback;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.setInterruptCallback.GatewayAppInterruptCallback:DA-END
    }
    
    /**
     *
     * @return
     */
    @Override
    public synchronized SoundSensorGroveData getData() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.getData.SoundSensorGroveData:DA-START
    	SoundSensorGroveData data = new SoundSensorGroveData();
    	data.setData(new SoundSensorGroveStatusType(mcp3008.readChannel(setup.getConnection().getChannel())));
    	return data;
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.getData.SoundSensorGroveData:DA-ELSE
        // TODO add code here to get the sensor data and return it 
        //return null;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.getData.SoundSensorGroveData:DA-END
    }
    
    /**
     */
    @Override
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.activate:DA-START
    	try {
    		mcp3008 = MCP3008.getInstance(MCP3008.MCP3008_CSPin, MCP3008.MCP3008_V_Ref);
    	}
    	catch (IOException e) {
    		throw new AnalogFaultException(e);
    	}
    	logger.debug("activated Sound Sensor Grove");
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.activate:DA-ELSE
        // TODO add code here to activate the hardware 
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.activate:DA-END
    }
    
    /**
     */
    @Override
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.deactivate:DA-START
    	try {
    		mcp3008.close();
    	}
    	catch (IOException e) {
    		throw new AnalogFaultException(e);
    	}
    	logger.debug("deactivated Temperature Sensor Grove");
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.deactivate:DA-ELSE
        // TODO add code here to deactivate the hardware 
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.deactivate:DA-END
    }
    
    
    public static class DeviceSetup { // start of class
    
        private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DeviceSetup.class);
        
        private AnalogConnection connection;
        
        /**
         * creates an instance of DeviceSetup
         *
         * @param connection  the connection
         */
        public DeviceSetup(AnalogConnection connection) {
            //DA-START:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.DeviceSetup.AnalogConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.DeviceSetup.AnalogConnection:DA-ELSE
            this.connection = connection;
            //DA-END:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.DeviceSetup.AnalogConnection:DA-END
        }
        
        
        /**
         * getter for the field connection
         *
         *
         *
         * @return
         */
        public AnalogConnection getConnection() {
            //DA-START:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.DeviceSetup.getConnection.AnalogConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.DeviceSetup.getConnection.AnalogConnection:DA-ELSE
            return this.connection;
            //DA-END:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.DeviceSetup.getConnection.AnalogConnection:DA-END
        }
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-END
    } // end of java type
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.additional.elements.in.type:DA-START
    private MCP3008 mcp3008;
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver.additional.elements.in.type:DA-END
} // end of java type