package org.gs.iot.sample.monitoring.gateway.driver;

import java.io.IOException;

import org.gs.iot.sample.iot.driver.lib.MCP3008;
import org.gs.iot.sample.monitoring.gateway.devicedata.AnalogConnection;
import org.gs.iot.sample.monitoring.gateway.devicedata.AnalogFaultException;
import org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData;
import org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveStatusType;
import org.gs.iot.sample.monitoring.gateway.driverimpl.AbstractGatewayAppDeviceDriver;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppInterruptCallback;
import org.slf4j.Logger;


public class TemperatureSensorGroveDeviceDriver extends AbstractGatewayAppDeviceDriver { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(TemperatureSensorGroveDeviceDriver.class);
    
    private TemperatureSensorGroveDeviceDriver.DeviceSetup setup;
    
    private GatewayAppInterruptCallback<TemperatureSensorGroveData> interruptCallback;
    
    /**
     * creates an instance of TemperatureSensorGroveDeviceDriver
     *
     * @param setup  the setup
     */
    public TemperatureSensorGroveDeviceDriver(TemperatureSensorGroveDeviceDriver.DeviceSetup setup) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup:DA-ELSE
        super();
        this.setup = setup;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup:DA-END
    }
    
    
    /**
     * getter for the field setup
     *
     *
     *
     * @return
     */
    public TemperatureSensorGroveDeviceDriver.DeviceSetup getSetup() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.getSetup.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.getSetup.DeviceSetup:DA-ELSE
        return this.setup;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.getSetup.DeviceSetup:DA-END
    }
    
    /**
     * setter for the field interruptCallback
     *
     *
     *
     * @param interruptCallback  the interruptCallback
     */
    public void setInterruptCallback(GatewayAppInterruptCallback<TemperatureSensorGroveData> interruptCallback) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.setInterruptCallback.GatewayAppInterruptCallback:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.setInterruptCallback.GatewayAppInterruptCallback:DA-ELSE
        this.interruptCallback = interruptCallback;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.setInterruptCallback.GatewayAppInterruptCallback:DA-END
    }
    
    /**
     *
     * @return
     */
    @Override
    public synchronized TemperatureSensorGroveData getData() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.getData.TemperatureSensorGroveData:DA-START
    	try {
    		int doc = mcp3008.readChannel(setup.getConnection().getChannel());
    		int B = 3975; // B value of the thermistor
    		float R = (1023f - doc) * 10000 / doc;
    		float T = 1f / (((float) Math.log(R / 10000) / B) + (1 / 298.15f));
    		float temp = T - 273.15f;
    		TemperatureSensorGroveData data = new TemperatureSensorGroveData();
    		data.setData(new TemperatureSensorGroveStatusType(temp));
    		return data;
    	}
    	catch (IOException e) {
    		throw new AnalogFaultException(e);
    	}
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.getData.TemperatureSensorGroveData:DA-ELSE
        // TODO add code here to get the sensor data and return it 
        //return null;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.getData.TemperatureSensorGroveData:DA-END
    }
    
    /**
     */
    @Override
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.activate:DA-START
    	try {
    		mcp3008 = MCP3008.getInstance(MCP3008.MCP3008_CSPin, MCP3008.MCP3008_V_Ref);
    	}
    	catch (IOException e) {
    		throw new AnalogFaultException(e);
    	}
    	logger.debug("activated Temperature Sensor Grove");
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.activate:DA-ELSE
        // TODO add code here to activate the hardware 
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.activate:DA-END
    }
    
    /**
     */
    @Override
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.deactivate:DA-START
    	try {
    		mcp3008.close();
    	}
    	catch (IOException e) {
    		throw new AnalogFaultException(e);
    	}
    	logger.debug("deactivated Temperature Sensor Grove");
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.deactivate:DA-ELSE
        // TODO add code here to deactivate the hardware 
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.deactivate:DA-END
    }
    
    
    public static class DeviceSetup { // start of class
    
        private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DeviceSetup.class);
        
        private AnalogConnection connection;
        
        /**
         * creates an instance of DeviceSetup
         *
         * @param connection  the connection
         */
        public DeviceSetup(AnalogConnection connection) {
            //DA-START:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup.AnalogConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup.AnalogConnection:DA-ELSE
            this.connection = connection;
            //DA-END:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup.AnalogConnection:DA-END
        }
        
        
        /**
         * getter for the field connection
         *
         *
         *
         * @return
         */
        public AnalogConnection getConnection() {
            //DA-START:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup.getConnection.AnalogConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup.getConnection.AnalogConnection:DA-ELSE
            return this.connection;
            //DA-END:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup.getConnection.AnalogConnection:DA-END
        }
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-END
    } // end of java type
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.additional.elements.in.type:DA-START
    private MCP3008 mcp3008;
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver.additional.elements.in.type:DA-END
} // end of java type