package org.gs.iot.sample.monitoring.gateway.driver;

import java.io.IOException;

import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalConnection;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalFaultException;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorStatusType;
import org.gs.iot.sample.monitoring.gateway.driverimpl.AbstractGatewayAppDeviceDriver;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppInterruptCallback;
import org.slf4j.Logger;

import jdk.dio.DeviceManager;
import jdk.dio.gpio.GPIOPin;
import jdk.dio.gpio.GPIOPinConfig;
import jdk.dio.gpio.PinEvent;
import jdk.dio.gpio.PinListener;


public class DigitalGroveSensorDeviceDriver extends AbstractGatewayAppDeviceDriver { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DigitalGroveSensorDeviceDriver.class);
    
    private DigitalGroveSensorDeviceDriver.DeviceSetup setup;
    
    private GatewayAppInterruptCallback<DigitalGroveSensorData> interruptCallback;
    
    /**
     * creates an instance of DigitalGroveSensorDeviceDriver
     *
     * @param setup  the setup
     */
    public DigitalGroveSensorDeviceDriver(DigitalGroveSensorDeviceDriver.DeviceSetup setup) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.DeviceSetup:DA-ELSE
        super();
        this.setup = setup;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.DeviceSetup:DA-END
    }
    
    
    /**
     * getter for the field setup
     *
     *
     *
     * @return
     */
    public DigitalGroveSensorDeviceDriver.DeviceSetup getSetup() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.getSetup.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.getSetup.DeviceSetup:DA-ELSE
        return this.setup;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.getSetup.DeviceSetup:DA-END
    }
    
    /**
     * setter for the field interruptCallback
     *
     *
     *
     * @param interruptCallback  the interruptCallback
     */
    public void setInterruptCallback(GatewayAppInterruptCallback<DigitalGroveSensorData> interruptCallback) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.setInterruptCallback.GatewayAppInterruptCallback:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.setInterruptCallback.GatewayAppInterruptCallback:DA-ELSE
        this.interruptCallback = interruptCallback;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.setInterruptCallback.GatewayAppInterruptCallback:DA-END
    }
    
    /**
     *
     * @return
     */
    @Override
    public synchronized DigitalGroveSensorData getData() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.getData.DigitalGroveSensorData:DA-START
    	try {
    		DigitalGroveSensorData data = new DigitalGroveSensorData();
			data.setData(new DigitalGroveSensorStatusType(pin.getValue()));
			return data;
    	} catch (IOException e) {
			throw new DigitalFaultException(e);
		}
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.getData.DigitalGroveSensorData:DA-ELSE
        // TODO add code here to get the sensor data and return it 
        //return null;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.getData.DigitalGroveSensorData:DA-END
    }
    
    /**
     */
    @Override
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.activate:DA-START
    	try {
    		GPIOPinConfig pinConfig = new GPIOPinConfig(0, setup.getConnection().getPinNumber(), GPIOPinConfig.DIR_INPUT_ONLY, GPIOPinConfig.DEFAULT, GPIOPinConfig.TRIGGER_BOTH_EDGES, false);
			pin = DeviceManager.open(GPIOPin.class, pinConfig);
			
			pin.setInputListener(new PinListener() {
				
				@Override
				public void valueChanged(PinEvent event) {
					DigitalGroveSensorData data = new DigitalGroveSensorData();
					data.setData(new DigitalGroveSensorStatusType(event.getValue()));
					interruptCallback.onInterrupt(data);
				}
			});
			
		} catch (IOException e) {
			throw new DigitalFaultException(e);
		}
    	logger.debug("activated PIR Motion Grove");
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.activate:DA-ELSE
        // TODO add code here to activate the hardware 
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.activate:DA-END
    }
    
    /**
     */
    @Override
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.deactivate:DA-START
    	try {
			pin.close();
		} catch (IOException e) {
//			throw new DigitalFaultException(e);
		}
    	pin = null;
    	logger.debug("deactivated PIR Motion Grove");
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.deactivate:DA-ELSE
        // TODO add code here to deactivate the hardware 
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.deactivate:DA-END
    }
    
    
    public static class DeviceSetup { // start of class
    
        private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DeviceSetup.class);
        
        private DigitalConnection connection;
        
        /**
         * creates an instance of DeviceSetup
         *
         * @param connection  the connection
         */
        public DeviceSetup(DigitalConnection connection) {
            //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.DeviceSetup.DigitalConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.DeviceSetup.DigitalConnection:DA-ELSE
            this.connection = connection;
            //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.DeviceSetup.DigitalConnection:DA-END
        }
        
        
        /**
         * getter for the field connection
         *
         *
         *
         * @return
         */
        public DigitalConnection getConnection() {
            //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.DeviceSetup.getConnection.DigitalConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.DeviceSetup.getConnection.DigitalConnection:DA-ELSE
            return this.connection;
            //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.DeviceSetup.getConnection.DigitalConnection:DA-END
        }
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.DeviceSetup.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.DeviceSetup.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.DeviceSetup.additional.elements.in.type:DA-END
    } // end of java type
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.additional.elements.in.type:DA-START
    private GPIOPin pin;
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver.additional.elements.in.type:DA-END
} // end of java type