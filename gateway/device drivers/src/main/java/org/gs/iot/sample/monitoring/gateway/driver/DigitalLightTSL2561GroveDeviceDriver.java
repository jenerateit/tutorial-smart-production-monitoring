package org.gs.iot.sample.monitoring.gateway.driver;

import java.io.IOException;

import org.gs.iot.sample.iot.driver.lib.DigitalLightTSL2561;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveStatusType;
import org.gs.iot.sample.monitoring.gateway.devicedata.I2cConnection;
import org.gs.iot.sample.monitoring.gateway.driverimpl.AbstractGatewayAppDeviceDriver;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppInterruptCallback;
import org.slf4j.Logger;


public class DigitalLightTSL2561GroveDeviceDriver extends AbstractGatewayAppDeviceDriver { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DigitalLightTSL2561GroveDeviceDriver.class);
    
    private DigitalLightTSL2561GroveDeviceDriver.DeviceSetup setup;
    
    private GatewayAppInterruptCallback<DigitalLightTSL2561GroveData> interruptCallback;
    
    /**
     * creates an instance of DigitalLightTSL2561GroveDeviceDriver
     *
     * @param setup  the setup
     */
    public DigitalLightTSL2561GroveDeviceDriver(DigitalLightTSL2561GroveDeviceDriver.DeviceSetup setup) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup:DA-ELSE
        super();
        this.setup = setup;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup:DA-END
    }
    
    
    /**
     * getter for the field setup
     *
     *
     *
     * @return
     */
    public DigitalLightTSL2561GroveDeviceDriver.DeviceSetup getSetup() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.getSetup.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.getSetup.DeviceSetup:DA-ELSE
        return this.setup;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.getSetup.DeviceSetup:DA-END
    }
    
    /**
     * setter for the field interruptCallback
     *
     *
     *
     * @param interruptCallback  the interruptCallback
     */
    public void setInterruptCallback(GatewayAppInterruptCallback<DigitalLightTSL2561GroveData> interruptCallback) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.setInterruptCallback.GatewayAppInterruptCallback:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.setInterruptCallback.GatewayAppInterruptCallback:DA-ELSE
        this.interruptCallback = interruptCallback;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.setInterruptCallback.GatewayAppInterruptCallback:DA-END
    }
    
    /**
     *
     * @return
     */
    @Override
    public synchronized DigitalLightTSL2561GroveData getData() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.getData.DigitalLightTSL2561GroveData:DA-START
    	DigitalLightTSL2561GroveData data = new DigitalLightTSL2561GroveData();
    	data.setData(new DigitalLightTSL2561GroveStatusType(light.getVisibleLux()));
    	return data;
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.getData.DigitalLightTSL2561GroveData:DA-ELSE
        // TODO add code here to get the sensor data and return it 
        //return null;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.getData.DigitalLightTSL2561GroveData:DA-END
    }
    
    /**
     */
    @Override
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.activate:DA-START
    	light = new DigitalLightTSL2561(DigitalLightTSL2561.Gain.x1, DigitalLightTSL2561.IntegrateTime.OneHoundredOne);
    	logger.debug("activated Digital Light TSL2561");
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.activate:DA-ELSE
        // TODO add code here to activate the hardware 
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.activate:DA-END
    }
    
    /**
     */
    @Override
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.deactivate:DA-START
    	light.close();
    	light = null;
    	logger.debug("deactivated Digital Light TSL2561");
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.deactivate:DA-ELSE
        // TODO add code here to deactivate the hardware 
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.deactivate:DA-END
    }
    
    
    public static class DeviceSetup { // start of class
    
        private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DeviceSetup.class);
        
        private I2cConnection connection;
        
        /**
         * creates an instance of DeviceSetup
         *
         * @param connection  the connection
         */
        public DeviceSetup(I2cConnection connection) {
            //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.I2cConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.I2cConnection:DA-ELSE
            this.connection = connection;
            //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.I2cConnection:DA-END
        }
        
        
        /**
         * getter for the field connection
         *
         *
         *
         * @return
         */
        public I2cConnection getConnection() {
            //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.getConnection.I2cConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.getConnection.I2cConnection:DA-ELSE
            return this.connection;
            //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.getConnection.I2cConnection:DA-END
        }
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-START
        private DigitalLightTSL2561 light;
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.DeviceSetup.additional.elements.in.type:DA-END
    } // end of java type
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.additional.elements.in.type:DA-START
    private DigitalLightTSL2561 light;
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver.additional.elements.in.type:DA-END
} // end of java type