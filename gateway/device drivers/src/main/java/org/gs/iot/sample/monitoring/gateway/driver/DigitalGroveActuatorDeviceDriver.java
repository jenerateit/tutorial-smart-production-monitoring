package org.gs.iot.sample.monitoring.gateway.driver;

import java.io.IOException;

import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalConnection;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalFaultException;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorStatusType;
import org.gs.iot.sample.monitoring.gateway.driverimpl.AbstractGatewayAppDeviceDriver;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppInterruptCallback;
import org.slf4j.Logger;

import jdk.dio.DeviceManager;
import jdk.dio.gpio.GPIOPin;
import jdk.dio.gpio.GPIOPinConfig;


public class DigitalGroveActuatorDeviceDriver extends AbstractGatewayAppDeviceDriver { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DigitalGroveActuatorDeviceDriver.class);
    
    private DigitalGroveActuatorDeviceDriver.DeviceSetup setup;
    
    private GatewayAppInterruptCallback<DigitalGroveActuatorData> interruptCallback;
    
    /**
     * creates an instance of DigitalGroveActuatorDeviceDriver
     *
     * @param setup  the setup
     */
    public DigitalGroveActuatorDeviceDriver(DigitalGroveActuatorDeviceDriver.DeviceSetup setup) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.DeviceSetup:DA-ELSE
        super();
        this.setup = setup;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.DeviceSetup:DA-END
    }
    
    
    /**
     * getter for the field setup
     *
     *
     *
     * @return
     */
    public DigitalGroveActuatorDeviceDriver.DeviceSetup getSetup() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.getSetup.DeviceSetup:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.getSetup.DeviceSetup:DA-ELSE
        return this.setup;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.getSetup.DeviceSetup:DA-END
    }
    
    /**
     * setter for the field interruptCallback
     *
     *
     *
     * @param interruptCallback  the interruptCallback
     */
    public void setInterruptCallback(GatewayAppInterruptCallback<DigitalGroveActuatorData> interruptCallback) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.setInterruptCallback.GatewayAppInterruptCallback:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.setInterruptCallback.GatewayAppInterruptCallback:DA-ELSE
        this.interruptCallback = interruptCallback;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.setInterruptCallback.GatewayAppInterruptCallback:DA-END
    }
    
    /**
     *
     * @param data
     */
    public synchronized void setData(DigitalGroveActuatorData data) throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.setData.DigitalGroveActuatorData:DA-START
    	try {
    		pin.setValue(data.getData().isStatus());
    		interruptCallback.onInterrupt(data);
    	} catch (IOException e) {
			throw new DigitalFaultException(e);
		}
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.setData.DigitalGroveActuatorData:DA-ELSE
        //return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.setData.DigitalGroveActuatorData:DA-END
    }
    
    /**
     *
     * @return
     */
    @Override
    public synchronized DigitalGroveActuatorData getData() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.getData.DigitalGroveActuatorData:DA-START
    	try {
    		DigitalGroveActuatorData data = new DigitalGroveActuatorData();
			data.setData(new DigitalGroveActuatorStatusType(pin.getValue()));
			return data;
    	} catch (IOException e) {
			throw new DigitalFaultException(e);
		}
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.getData.DigitalGroveActuatorData:DA-ELSE
        // TODO add code here to get the sensor data and return it 
        //return null;
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.getData.DigitalGroveActuatorData:DA-END
    }
    
    /**
     */
    @Override
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.activate:DA-START
    	try {
    		GPIOPinConfig pinConfig = new GPIOPinConfig(0, setup.getConnection().getPinNumber(), GPIOPinConfig.DIR_OUTPUT_ONLY, GPIOPinConfig.DEFAULT, GPIOPinConfig.TRIGGER_BOTH_EDGES, false);
			pin = DeviceManager.open(GPIOPin.class, pinConfig);
		} catch (IOException e) {
			throw new DigitalFaultException(e);
		}
    	logger.debug("activated digital actuator");
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.activate:DA-ELSE
        // TODO add code here to activate the hardware 
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.activate:DA-END
    }
    
    /**
     */
    @Override
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.deactivate:DA-START
    	try {
			pin.close();
		} catch (IOException e) {
			throw new DigitalFaultException(e);
		}
    	pin = null;
    	logger.debug("deactivated digital actuator");
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.deactivate:DA-ELSE
        // TODO add code here to deactivate the hardware 
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.deactivate:DA-END
    }
    
    
    public static class DeviceSetup { // start of class
    
        private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DeviceSetup.class);
        
        private DigitalConnection connection;
        
        /**
         * creates an instance of DeviceSetup
         *
         * @param connection  the connection
         */
        public DeviceSetup(DigitalConnection connection) {
            //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.DeviceSetup.DigitalConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.DeviceSetup.DigitalConnection:DA-ELSE
            this.connection = connection;
            //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.DeviceSetup.DigitalConnection:DA-END
        }
        
        
        /**
         * getter for the field connection
         *
         *
         *
         * @return
         */
        public DigitalConnection getConnection() {
            //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.DeviceSetup.getConnection.DigitalConnection:DA-START
            //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.DeviceSetup.getConnection.DigitalConnection:DA-ELSE
            return this.connection;
            //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.DeviceSetup.getConnection.DigitalConnection:DA-END
        }
        //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.DeviceSetup.additional.elements.in.type:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.DeviceSetup.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.DeviceSetup.additional.elements.in.type:DA-END
    } // end of java type
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.additional.elements.in.type:DA-START
    private GPIOPin pin;
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver.additional.elements.in.type:DA-END
} // end of java type