package org.gs.iot.sample.iot.driver.lib;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jdk.dio.DeviceManager;
import jdk.dio.i2cbus.I2CDevice;
import jdk.dio.i2cbus.I2CDeviceConfig;

public class TH02 {
	private static final int TH02_ADDRESS = 0x40;
	// private static final DecimalFormat TWO_DECIMALS_FORMAT = new DecimalFormat("#.##");

	private static final Logger logger = LoggerFactory.getLogger(TH02.class);


	private I2CDevice i2cDevice;


	public TH02() throws IOException {
		I2CDeviceConfig i2cDeviceConfig = new I2CDeviceConfig(1, TH02_ADDRESS, 7, I2CDeviceUtil.I2C_CLOCK_FREQUENCY);
		i2cDevice = DeviceManager.open(i2cDeviceConfig);
	}

	public void close() throws IOException {
		i2cDevice.close();
	}
	
	/**
	 * See sensor documentation here:
	 * http://www.hoperf.cn/upload/sensor/TH02_V1.1.pdf, chapter 3 (Host Interface)
	 * 
	 * @return two decimal format float
	 * @throws IOException
	 */
	public float getTemperature() throws IOException {
		try {
			i2cDevice.tryLock(I2CDeviceUtil.I2C_TIMEOUT);
			float TEMP = 0.0f;
			float temperature = 0.0f;

			// Set START (D0) and TEMP (D4) in CONFIG (register 0x03) to begin a
			// new conversion, i.e., write CONFIG with 0x11
			I2CDeviceUtil.write(i2cDevice, 0x03, 0x11);
			
			// Poll RDY (D0) in STATUS (register 0) until it is low (=0)
			int status = -1;
			while((status & 0x01) != 0) { //TODO maybe kills bus, while iterating?
				status = I2CDeviceUtil.read(i2cDevice, 0x00);
			}

			// Read the upper and lower bytes of the temperature value from
			// DATAh and DATAl (registers 0x01 and 0x02), respectively
			byte[] buffer = new byte[3];
			I2CDeviceUtil.read(i2cDevice, buffer, 0, 3);

			int dataH = buffer[1] & 0xff;
			int dataL = buffer[2] & 0xff;

			TEMP = (dataH * 256 + dataL) >> 2;
			temperature = (TEMP / 32f) - 50f;
			return temperature;
		}
		finally {
			i2cDevice.unlock();
		}
	}

	/**
	 * See sensor documentation here:
	 * http://www.hoperf.cn/upload/sensor/TH02_V1.1.pdf, chapter 3 (Host Interface)
	 * 
	 * @return
	 * @throws IOException
	 */
	public float getHumidity() throws IOException {
		try {
			i2cDevice.tryLock(I2CDeviceUtil.I2C_TIMEOUT);
			float RH = 0.0f;
			float relativeHumidity = 0.0f;

			// Set START (D0) in CONFIG to begin a new conversion
			I2CDeviceUtil.write(i2cDevice, 0x03, 0x01);

			// Poll RDY (D0) in STATUS (register 0) until it is low (= 0)
			int status = -1;
			while((status & 0x01) != 0) {
				status = I2CDeviceUtil.read(i2cDevice, 0x00);
			}

			// Read the upper and lower bytes of the RH value from
			// DATAh and DATAl (registers 0x01 and 0x02), respectively
			byte[] buffer = new byte[3];
			I2CDeviceUtil.read(i2cDevice, buffer, 0, 3);

			int dataH = buffer[1] & 0xff;
			int dataL = buffer[2] & 0xff;

			RH = (dataH * 256 + dataL) >> 4;
			relativeHumidity = (RH / 16f) - 24f;
			return relativeHumidity;
		}
		finally {
			i2cDevice.unlock();
		}
	}
}
