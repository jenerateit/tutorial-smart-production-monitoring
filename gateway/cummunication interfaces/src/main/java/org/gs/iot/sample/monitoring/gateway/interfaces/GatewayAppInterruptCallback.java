package org.gs.iot.sample.monitoring.gateway.interfaces;
import org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData;


public interface GatewayAppInterruptCallback<T extends AbstractGatewayAppDeviceData> { // start of interface

    
    /**
     *
     * @param data  the data
     */
    void onInterrupt(T data);
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppInterruptCallback.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppInterruptCallback.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppInterruptCallback.additional.elements.in.type:DA-END
} // end of java type