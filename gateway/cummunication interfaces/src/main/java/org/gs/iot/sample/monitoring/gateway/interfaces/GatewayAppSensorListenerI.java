package org.gs.iot.sample.monitoring.gateway.interfaces;
import org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData;


public interface GatewayAppSensorListenerI<T extends AbstractGatewayAppDeviceData> { // start of interface

    
    /**
     *
     * @param deviceConfiguration  the deviceConfiguration
     * @param deviceData  the deviceData
     */
    void onChange(GatewayAppDeviceConfigEnum deviceConfiguration, T deviceData);
    
    /**
     *
     * @param deviceConfiguration  the deviceConfiguration
     * @param th  the th
     */
    void onError(GatewayAppDeviceConfigEnum deviceConfiguration, Throwable th);
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppSensorListenerI.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppSensorListenerI.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppSensorListenerI.additional.elements.in.type:DA-END
} // end of java type