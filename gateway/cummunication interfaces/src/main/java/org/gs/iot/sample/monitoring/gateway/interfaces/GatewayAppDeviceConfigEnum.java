package org.gs.iot.sample.monitoring.gateway.interfaces;


import org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData;



public enum GatewayAppDeviceConfigEnum { // start of class
    
    MOTION("Motion", "DigitalGroveSensor", null, "motion", org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.class),
    
    SOUND("Sound", "SoundSensorGrove", null, "soundsensor", org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveData.class),
    
    ALARM("Alarm", "DigitalGroveActuator", null, "alarm", org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.class),
    
    LIGHT("Light", "DigitalGroveActuator", null, "light", org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.class),
    
    ILLUMINANCE("Illuminance", "DigitalLightTSL2561Grove", null, "illuminance", org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.class),
    
    TEMPERATURE("Temperature", "TemperatureSensorGrove", null, "temperature", org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.class)
    //DA-START:interfaces.GatewayAppDeviceConfigEnum.enum.constants:DA-START
    //DA-ELSE:interfaces.GatewayAppDeviceConfigEnum.enum.constants:DA-ELSE
    // add additional enum entries here 
    //DA-END:interfaces.GatewayAppDeviceConfigEnum.enum.constants:DA-END
    ;
    
    private final String name;
    
    private final String deviceId;
    
    private final String deviceVersion;
    
    private final String resource;
    
    private final Class<? extends AbstractGatewayAppDeviceData> dataClass;
    
    
    /**
     * creates an instance of GatewayAppDeviceConfigEnum
     *
     * @param name  the name
     * @param deviceId  the deviceId
     * @param deviceVersion  the deviceVersion
     * @param resource  the resource
     * @param dataClass  the dataClass
     */
    private GatewayAppDeviceConfigEnum(String name, String deviceId, String deviceVersion, String resource, Class<? extends AbstractGatewayAppDeviceData> dataClass) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.String.String.String.String.Class:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.String.String.String.String.Class:DA-ELSE
        this.name = name;
        this.deviceId = deviceId;
        this.deviceVersion = deviceVersion;
        this.resource = resource;
        this.dataClass = dataClass;
        //DA-END:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.String.String.String.String.Class:DA-END
    }
    
    
    
    /**
     * getter for the field name
     *
     *
     *
     * @return
     */
    public String getName() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.getName.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.getName.String:DA-ELSE
        return this.name;
        //DA-END:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.getName.String:DA-END
    }
    
    /**
     * getter for the field deviceId
     *
     *
     *
     * @return
     */
    public String getDeviceId() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.getDeviceId.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.getDeviceId.String:DA-ELSE
        return this.deviceId;
        //DA-END:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.getDeviceId.String:DA-END
    }
    
    /**
     * getter for the field deviceVersion
     *
     *
     *
     * @return
     */
    public String getDeviceVersion() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.getDeviceVersion.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.getDeviceVersion.String:DA-ELSE
        return this.deviceVersion;
        //DA-END:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.getDeviceVersion.String:DA-END
    }
    
    /**
     * getter for the field resource
     *
     *
     *
     * @return
     */
    public String getResource() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.getResource.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.getResource.String:DA-ELSE
        return this.resource;
        //DA-END:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.getResource.String:DA-END
    }
    
    /**
     * getter for the field dataClass
     *
     *
     *
     * @return
     */
    public Class<? extends AbstractGatewayAppDeviceData> getDataClass() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.getDataClass.Class:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.getDataClass.Class:DA-ELSE
        return this.dataClass;
        //DA-END:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.getDataClass.Class:DA-END
    }
    
    
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum.additional.elements.in.type:DA-END
} // end of java type