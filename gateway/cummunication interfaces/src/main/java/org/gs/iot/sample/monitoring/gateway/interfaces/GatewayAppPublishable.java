package org.gs.iot.sample.monitoring.gateway.interfaces;


public interface GatewayAppPublishable { // start of interface

    
    /**
     *
     * @param resource  the resource
     * @param payload  the payload
     */
    void publish(String resource, byte[] payload);
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppPublishable.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppPublishable.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppPublishable.additional.elements.in.type:DA-END
} // end of java type