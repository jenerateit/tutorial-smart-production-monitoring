package org.gs.iot.sample.monitoring.gateway.interfaces;
import java.util.Collection;

import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData;
import org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData;

import org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveData;


public interface GatewayAppDeviceServiceI { // start of interface

    
    /**
     *
     * @return
     */
    DigitalGroveSensorData getMotion();
    
    /**
     *
     * @return
     */
    DigitalGroveSensorData getMotionLastSentData();
    
    /**
     *
     * @return
     */
    Collection<DigitalGroveSensorData> getMotionHistoryData();
    
    /**
     *
     * @return
     */
    SoundSensorGroveData getSound();
    
    /**
     *
     * @return
     */
    SoundSensorGroveData getSoundLastSentData();
    
    /**
     *
     * @return
     */
    Collection<SoundSensorGroveData> getSoundHistoryData();
    
    /**
     *
     * @return
     */
    DigitalGroveActuatorData getAlarm();
    
    /**
     *
     * @return
     */
    DigitalGroveActuatorData getAlarmLastSentData();
    
    /**
     *
     * @return
     */
    Collection<DigitalGroveActuatorData> getAlarmHistoryData();
    
    /**
     *
     * @return
     */
    DigitalGroveActuatorData getLight();
    
    /**
     *
     * @return
     */
    DigitalGroveActuatorData getLightLastSentData();
    
    /**
     *
     * @return
     */
    Collection<DigitalGroveActuatorData> getLightHistoryData();
    
    /**
     *
     * @return
     */
    DigitalLightTSL2561GroveData getIlluminance();
    
    /**
     *
     * @return
     */
    DigitalLightTSL2561GroveData getIlluminanceLastSentData();
    
    /**
     *
     * @return
     */
    Collection<DigitalLightTSL2561GroveData> getIlluminanceHistoryData();
    
    /**
     *
     * @return
     */
    TemperatureSensorGroveData getTemperature();
    
    /**
     *
     * @return
     */
    TemperatureSensorGroveData getTemperatureLastSentData();
    
    /**
     *
     * @return
     */
    Collection<TemperatureSensorGroveData> getTemperatureHistoryData();
    
    /**
     *
     * @param deviceData  the deviceData
     */
    void setAlarm(DigitalGroveActuatorData deviceData);
    
    /**
     *
     * @param deviceData  the deviceData
     */
    void setLight(DigitalGroveActuatorData deviceData);
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceServiceI.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceServiceI.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceServiceI.additional.elements.in.type:DA-END
} // end of java type