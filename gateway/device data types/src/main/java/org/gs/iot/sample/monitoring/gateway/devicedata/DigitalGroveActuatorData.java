package org.gs.iot.sample.monitoring.gateway.devicedata;

import org.slf4j.Logger;


public class DigitalGroveActuatorData extends AbstractGatewayAppDeviceData { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DigitalGroveActuatorData.class);
    
    private DigitalGroveActuatorStatusType data;
    
    private DigitalFaultException fault;
    
    
    /**
     * getter for the field data
     *
     *
     *
     * @return
     */
    public DigitalGroveActuatorStatusType getData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.getData.DigitalGroveActuatorStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.getData.DigitalGroveActuatorStatusType:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.getData.DigitalGroveActuatorStatusType:DA-END
    }
    
    /**
     * setter for the field data
     *
     *
     *
     * @param data  the data
     */
    public void setData(DigitalGroveActuatorStatusType data) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.setData.DigitalGroveActuatorStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.setData.DigitalGroveActuatorStatusType:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.setData.DigitalGroveActuatorStatusType:DA-END
    }
    
    /**
     * getter for the field fault
     *
     *
     *
     * @return
     */
    public DigitalFaultException getFault() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.getFault.DigitalFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.getFault.DigitalFaultException:DA-ELSE
        return this.fault;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.getFault.DigitalFaultException:DA-END
    }
    
    /**
     * setter for the field fault
     *
     *
     *
     * @param fault  the fault
     */
    public void setFault(DigitalFaultException fault) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.setFault.DigitalFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.setFault.DigitalFaultException:DA-ELSE
        this.fault = fault;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.setFault.DigitalFaultException:DA-END
    }
    
    /**
     *
     * @param prettyPrinting  the prettyPrinting
     * @return
     */
    public byte[] getPayload(boolean prettyPrinting) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.getPayload.boolean.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.getPayload.boolean.byte.ARRAY:DA-ELSE
        return super.getPayload(prettyPrinting);
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.getPayload.boolean.byte.ARRAY:DA-END
    }
    
    /**
     *
     * @return
     */
    public byte[] getPayload() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.getPayload.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.getPayload.byte.ARRAY:DA-ELSE
        return super.getPayload();
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.getPayload.byte.ARRAY:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData.additional.elements.in.type:DA-END
} // end of java type