package org.gs.iot.sample.monitoring.gateway.devicedata;

import org.slf4j.Logger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.nio.charset.StandardCharsets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;


public abstract class AbstractGatewayAppDeviceData { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(AbstractGatewayAppDeviceData.class);
    
    public static final SimpleDateFormat DATE_FORMAT;
    
    private Date timestamp = new Date();
    
    static {
        TimeZone tz = TimeZone.getDefault();
        DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
        DATE_FORMAT.setTimeZone(tz);
    }
    
    
    /**
     * getter for the field timestamp
     *
     *
     *
     * @return
     */
    public Date getTimestamp() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData.getTimestamp.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData.getTimestamp.Date:DA-ELSE
        return this.timestamp;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData.getTimestamp.Date:DA-END
    }
    
    /**
     * setter for the field timestamp
     *
     *
     *
     * @param timestamp  the timestamp
     */
    public void setTimestamp(Date timestamp) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData.setTimestamp.Date:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData.setTimestamp.Date:DA-ELSE
        this.timestamp = timestamp;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData.setTimestamp.Date:DA-END
    }
    
    /**
     *
     * @param clazz  the clazz
     * @param payload  the payload
     * @return
     */
    public static <T extends AbstractGatewayAppDeviceData> T createInstance(Class<T> clazz, byte[] payload) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData.createInstance.Class.byte.ARRAY.T:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData.createInstance.Class.byte.ARRAY.T:DA-ELSE
        String json = new String(payload, StandardCharsets.UTF_8);
        Gson gson = new GsonBuilder().setDateFormat(DATE_FORMAT.toPattern()).create();
        T data = gson.fromJson(json, clazz);
        return data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData.createInstance.Class.byte.ARRAY.T:DA-END
    }
    
    /**
     *
     * @param prettyPrinting  the prettyPrinting
     * @return
     */
    public byte[] getPayload(boolean prettyPrinting) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData.getPayload.boolean.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData.getPayload.boolean.byte.ARRAY:DA-ELSE
        Gson gson = null;
        if(prettyPrinting) {
        	gson = new GsonBuilder().setPrettyPrinting().setDateFormat(DATE_FORMAT.toPattern()).create();
        }
        else {
        	gson = new GsonBuilder().setDateFormat(DATE_FORMAT.toPattern()).create();
        }
        
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("data", gson.toJsonTree(getData()));
        jsonObject.add("fault", gson.toJsonTree(getFault()));
        jsonObject.add("timestamp", gson.toJsonTree(this.timestamp));
        
        
        String json = gson.toJson(jsonObject);
        return json.getBytes(StandardCharsets.UTF_8);
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData.getPayload.boolean.byte.ARRAY:DA-END
    }
    
    /**
     *
     * @return
     */
    public byte[] getPayload() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData.getPayload.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData.getPayload.byte.ARRAY:DA-ELSE
        return getPayload(true);
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData.getPayload.byte.ARRAY:DA-END
    }
    
    /**
     *
     * @return
     */
    public abstract Object getData();
    
    /**
     *
     * @return
     */
    public abstract Object getFault();
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData.additional.elements.in.type:DA-END
} // end of java type