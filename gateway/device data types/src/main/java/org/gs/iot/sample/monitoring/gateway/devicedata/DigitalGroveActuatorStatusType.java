package org.gs.iot.sample.monitoring.gateway.devicedata;



public class DigitalGroveActuatorStatusType { // start of class

    private boolean status;
    
    /**
     * creates an instance of DigitalGroveActuatorStatusType
     *
     * @param status  the status
     */
    public DigitalGroveActuatorStatusType(boolean status) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorStatusType.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorStatusType.boolean:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorStatusType.boolean:DA-END
    }
    
    /**
     * creates an instance of DigitalGroveActuatorStatusType
     */
    public DigitalGroveActuatorStatusType() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorStatusType:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorStatusType:DA-END
    }
    
    
    /**
     * getter for the field status
     *
     *
     *
     * @return
     */
    public boolean isStatus() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorStatusType.isStatus.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorStatusType.isStatus.boolean:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorStatusType.isStatus.boolean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorStatusType.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorStatusType.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorStatusType.additional.elements.in.type:DA-END
} // end of java type