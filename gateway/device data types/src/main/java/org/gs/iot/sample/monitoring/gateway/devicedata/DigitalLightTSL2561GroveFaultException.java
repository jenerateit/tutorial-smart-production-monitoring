package org.gs.iot.sample.monitoring.gateway.devicedata;

import java.io.IOException;


/**
 * Digital Light TSL2561 Grove
 */
public class DigitalLightTSL2561GroveFaultException extends IOException { // start of class

    private static final long serialVersionUID = 1L;
    
    /**
     * creates an instance of DigitalLightTSL2561GroveFaultException
     *
     * @param arg0  the arg0
     */
    public DigitalLightTSL2561GroveFaultException(Throwable arg0) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveFaultException.Throwable:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveFaultException.Throwable:DA-ELSE
        super(arg0);
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveFaultException.Throwable:DA-END
    }
    
    /**
     * creates an instance of DigitalLightTSL2561GroveFaultException
     *
     * @param arg0  the arg0
     * @param arg1  the arg1
     */
    public DigitalLightTSL2561GroveFaultException(String arg0, Throwable arg1) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveFaultException.String.Throwable:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveFaultException.String.Throwable:DA-ELSE
        super(arg0, arg1);
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveFaultException.String.Throwable:DA-END
    }
    
    /**
     * creates an instance of DigitalLightTSL2561GroveFaultException
     *
     * @param arg0  the arg0
     */
    public DigitalLightTSL2561GroveFaultException(String arg0) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveFaultException.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveFaultException.String:DA-ELSE
        super(arg0);
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveFaultException.String:DA-END
    }
    
    /**
     * creates an instance of DigitalLightTSL2561GroveFaultException
     */
    public DigitalLightTSL2561GroveFaultException() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveFaultException:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveFaultException:DA-END
    }
    
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveFaultException.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveFaultException.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveFaultException.additional.elements.in.type:DA-END
} // end of java type