package org.gs.iot.sample.monitoring.gateway.devicedata;



public class DigitalConnection { // start of class

    private int pinNumber;
    
    private int trigger;
    
    
    /**
     * getter for the field pinNumber
     *
     *
     *
     * @return
     */
    public int getPinNumber() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalConnection.getPinNumber.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalConnection.getPinNumber.int:DA-ELSE
        return this.pinNumber;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalConnection.getPinNumber.int:DA-END
    }
    
    /**
     * setter for the field pinNumber
     *
     *
     *
     * @param pinNumber  the pinNumber
     */
    public void setPinNumber(int pinNumber) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalConnection.setPinNumber.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalConnection.setPinNumber.int:DA-ELSE
        this.pinNumber = pinNumber;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalConnection.setPinNumber.int:DA-END
    }
    
    /**
     * getter for the field trigger
     *
     *
     *
     * @return
     */
    public int getTrigger() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalConnection.getTrigger.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalConnection.getTrigger.int:DA-ELSE
        return this.trigger;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalConnection.getTrigger.int:DA-END
    }
    
    /**
     * setter for the field trigger
     *
     *
     *
     * @param trigger  the trigger
     */
    public void setTrigger(int trigger) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalConnection.setTrigger.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalConnection.setTrigger.int:DA-ELSE
        this.trigger = trigger;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalConnection.setTrigger.int:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalConnection.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalConnection.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalConnection.additional.elements.in.type:DA-END
} // end of java type