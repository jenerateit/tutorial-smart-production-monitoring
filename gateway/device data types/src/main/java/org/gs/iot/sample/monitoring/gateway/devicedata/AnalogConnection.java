package org.gs.iot.sample.monitoring.gateway.devicedata;



public class AnalogConnection { // start of class

    private int channel;
    
    
    /**
     * getter for the field channel
     *
     *
     *
     * @return
     */
    public int getChannel() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.AnalogConnection.getChannel.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.AnalogConnection.getChannel.int:DA-ELSE
        return this.channel;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.AnalogConnection.getChannel.int:DA-END
    }
    
    /**
     * setter for the field channel
     *
     *
     *
     * @param channel  the channel
     */
    public void setChannel(int channel) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.AnalogConnection.setChannel.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.AnalogConnection.setChannel.int:DA-ELSE
        this.channel = channel;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.AnalogConnection.setChannel.int:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.AnalogConnection.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.AnalogConnection.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.AnalogConnection.additional.elements.in.type:DA-END
} // end of java type