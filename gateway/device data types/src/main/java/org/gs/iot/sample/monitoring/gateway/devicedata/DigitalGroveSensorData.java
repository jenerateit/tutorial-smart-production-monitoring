package org.gs.iot.sample.monitoring.gateway.devicedata;

import org.slf4j.Logger;


public class DigitalGroveSensorData extends AbstractGatewayAppDeviceData { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DigitalGroveSensorData.class);
    
    private DigitalGroveSensorStatusType data;
    
    private DigitalFaultException fault;
    
    
    /**
     * getter for the field data
     *
     *
     *
     * @return
     */
    public DigitalGroveSensorStatusType getData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.getData.DigitalGroveSensorStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.getData.DigitalGroveSensorStatusType:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.getData.DigitalGroveSensorStatusType:DA-END
    }
    
    /**
     * setter for the field data
     *
     *
     *
     * @param data  the data
     */
    public void setData(DigitalGroveSensorStatusType data) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.setData.DigitalGroveSensorStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.setData.DigitalGroveSensorStatusType:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.setData.DigitalGroveSensorStatusType:DA-END
    }
    
    /**
     * getter for the field fault
     *
     *
     *
     * @return
     */
    public DigitalFaultException getFault() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.getFault.DigitalFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.getFault.DigitalFaultException:DA-ELSE
        return this.fault;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.getFault.DigitalFaultException:DA-END
    }
    
    /**
     * setter for the field fault
     *
     *
     *
     * @param fault  the fault
     */
    public void setFault(DigitalFaultException fault) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.setFault.DigitalFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.setFault.DigitalFaultException:DA-ELSE
        this.fault = fault;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.setFault.DigitalFaultException:DA-END
    }
    
    /**
     *
     * @param prettyPrinting  the prettyPrinting
     * @return
     */
    public byte[] getPayload(boolean prettyPrinting) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.getPayload.boolean.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.getPayload.boolean.byte.ARRAY:DA-ELSE
        return super.getPayload(prettyPrinting);
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.getPayload.boolean.byte.ARRAY:DA-END
    }
    
    /**
     *
     * @return
     */
    public byte[] getPayload() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.getPayload.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.getPayload.byte.ARRAY:DA-ELSE
        return super.getPayload();
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.getPayload.byte.ARRAY:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData.additional.elements.in.type:DA-END
} // end of java type