package org.gs.iot.sample.monitoring.gateway.devicedata;



public class DigitalGroveSensorStatusType { // start of class

    private boolean status;
    
    /**
     * creates an instance of DigitalGroveSensorStatusType
     *
     * @param status  the status
     */
    public DigitalGroveSensorStatusType(boolean status) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorStatusType.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorStatusType.boolean:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorStatusType.boolean:DA-END
    }
    
    /**
     * creates an instance of DigitalGroveSensorStatusType
     */
    public DigitalGroveSensorStatusType() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorStatusType:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorStatusType:DA-END
    }
    
    
    /**
     * getter for the field status
     *
     *
     *
     * @return
     */
    public boolean isStatus() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorStatusType.isStatus.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorStatusType.isStatus.boolean:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorStatusType.isStatus.boolean:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorStatusType.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorStatusType.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorStatusType.additional.elements.in.type:DA-END
} // end of java type