package org.gs.iot.sample.monitoring.gateway.devicedata;



public class SoundSensorGroveStatusType { // start of class

    private int status;
    
    /**
     * creates an instance of SoundSensorGroveStatusType
     *
     * @param status  the status
     */
    public SoundSensorGroveStatusType(int status) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveStatusType.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveStatusType.int:DA-ELSE
        this.status = status;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveStatusType.int:DA-END
    }
    
    /**
     * creates an instance of SoundSensorGroveStatusType
     */
    public SoundSensorGroveStatusType() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveStatusType:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveStatusType:DA-END
    }
    
    
    /**
     * getter for the field status
     *
     *
     *
     * @return
     */
    public int getStatus() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveStatusType.getStatus.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveStatusType.getStatus.int:DA-ELSE
        return this.status;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveStatusType.getStatus.int:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveStatusType.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveStatusType.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveStatusType.additional.elements.in.type:DA-END
} // end of java type