package org.gs.iot.sample.monitoring.gateway.devicedata;

import org.slf4j.Logger;


public class TemperatureSensorGroveData extends AbstractGatewayAppDeviceData { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(TemperatureSensorGroveData.class);
    
    private TemperatureSensorGroveStatusType data;
    
    private AnalogFaultException fault;
    
    
    /**
     * getter for the field data
     *
     *
     *
     * @return
     */
    public TemperatureSensorGroveStatusType getData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.getData.TemperatureSensorGroveStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.getData.TemperatureSensorGroveStatusType:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.getData.TemperatureSensorGroveStatusType:DA-END
    }
    
    /**
     * setter for the field data
     *
     *
     *
     * @param data  the data
     */
    public void setData(TemperatureSensorGroveStatusType data) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.setData.TemperatureSensorGroveStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.setData.TemperatureSensorGroveStatusType:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.setData.TemperatureSensorGroveStatusType:DA-END
    }
    
    /**
     * getter for the field fault
     *
     *
     *
     * @return
     */
    public AnalogFaultException getFault() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.getFault.AnalogFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.getFault.AnalogFaultException:DA-ELSE
        return this.fault;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.getFault.AnalogFaultException:DA-END
    }
    
    /**
     * setter for the field fault
     *
     *
     *
     * @param fault  the fault
     */
    public void setFault(AnalogFaultException fault) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.setFault.AnalogFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.setFault.AnalogFaultException:DA-ELSE
        this.fault = fault;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.setFault.AnalogFaultException:DA-END
    }
    
    /**
     *
     * @param prettyPrinting  the prettyPrinting
     * @return
     */
    public byte[] getPayload(boolean prettyPrinting) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.getPayload.boolean.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.getPayload.boolean.byte.ARRAY:DA-ELSE
        return super.getPayload(prettyPrinting);
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.getPayload.boolean.byte.ARRAY:DA-END
    }
    
    /**
     *
     * @return
     */
    public byte[] getPayload() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.getPayload.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.getPayload.byte.ARRAY:DA-ELSE
        return super.getPayload();
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.getPayload.byte.ARRAY:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData.additional.elements.in.type:DA-END
} // end of java type