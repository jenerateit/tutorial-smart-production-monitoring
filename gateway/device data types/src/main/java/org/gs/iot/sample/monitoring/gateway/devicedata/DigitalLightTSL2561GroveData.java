package org.gs.iot.sample.monitoring.gateway.devicedata;

import org.slf4j.Logger;


public class DigitalLightTSL2561GroveData extends AbstractGatewayAppDeviceData { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(DigitalLightTSL2561GroveData.class);
    
    private DigitalLightTSL2561GroveStatusType data;
    
    private DigitalLightTSL2561GroveFaultException fault;
    
    
    /**
     * getter for the field data
     *
     *
     *
     * @return
     */
    public DigitalLightTSL2561GroveStatusType getData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.getData.DigitalLightTSL2561GroveStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.getData.DigitalLightTSL2561GroveStatusType:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.getData.DigitalLightTSL2561GroveStatusType:DA-END
    }
    
    /**
     * setter for the field data
     *
     *
     *
     * @param data  the data
     */
    public void setData(DigitalLightTSL2561GroveStatusType data) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.setData.DigitalLightTSL2561GroveStatusType:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.setData.DigitalLightTSL2561GroveStatusType:DA-ELSE
        this.data = data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.setData.DigitalLightTSL2561GroveStatusType:DA-END
    }
    
    /**
     * getter for the field fault
     *
     *
     *
     * @return
     */
    public DigitalLightTSL2561GroveFaultException getFault() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.getFault.DigitalLightTSL2561GroveFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.getFault.DigitalLightTSL2561GroveFaultException:DA-ELSE
        return this.fault;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.getFault.DigitalLightTSL2561GroveFaultException:DA-END
    }
    
    /**
     * setter for the field fault
     *
     *
     *
     * @param fault  the fault
     */
    public void setFault(DigitalLightTSL2561GroveFaultException fault) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.setFault.DigitalLightTSL2561GroveFaultException:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.setFault.DigitalLightTSL2561GroveFaultException:DA-ELSE
        this.fault = fault;
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.setFault.DigitalLightTSL2561GroveFaultException:DA-END
    }
    
    /**
     *
     * @param prettyPrinting  the prettyPrinting
     * @return
     */
    public byte[] getPayload(boolean prettyPrinting) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.getPayload.boolean.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.getPayload.boolean.byte.ARRAY:DA-ELSE
        return super.getPayload(prettyPrinting);
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.getPayload.boolean.byte.ARRAY:DA-END
    }
    
    /**
     *
     * @return
     */
    public byte[] getPayload() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.getPayload.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.getPayload.byte.ARRAY:DA-ELSE
        return super.getPayload();
        //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.getPayload.byte.ARRAY:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData.additional.elements.in.type:DA-END
} // end of java type