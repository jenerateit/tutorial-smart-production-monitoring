package org.gs.iot.sample.monitoring.gateway.logicimpl;

import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppSensorListenerI;
import org.slf4j.Logger;
import java.util.Map;
import org.gs.iot.sample.monitoring.gateway.logic.LightControlBusinessLogicI;
import org.osgi.service.component.ComponentContext;
import org.osgi.framework.BundleContext;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum;


public class IlluminanceListener<T extends DigitalLightTSL2561GroveData> implements GatewayAppSensorListenerI<T> { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(IlluminanceListener.class);
    
    /**
     * holds configuration properties for the bundle or service
     */
    private Map<String, Object> properties;
    
    private LightControlBusinessLogicI gatewayLogicExecutorLightControlBusinessLogic;
    
    /**
     * creates an instance of IlluminanceListener
     */
    public IlluminanceListener() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener:DA-END
    }
    
    
    /**
     *
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void activate(ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.activate.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.activate.ComponentContext.BundleContext.Map:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.activate.ComponentContext.BundleContext.Map:DA-END
    }
    
    /**
     *
     * @param componentContext  the componentContext
     */
    protected void modified(ComponentContext componentContext) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.modified.ComponentContext:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.modified.ComponentContext:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.modified.ComponentContext:DA-END
    }
    
    /**
     *
     * @param reason  the reason
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.deactivate.Integer.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.deactivate.Integer.ComponentContext.BundleContext.Map:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.deactivate.Integer.ComponentContext.BundleContext.Map:DA-END
    }
    
    /**
     *
     * @param deviceConfiguration  the deviceConfiguration
     * @param deviceData  the deviceData
     */
    @Override
    public void onChange(GatewayAppDeviceConfigEnum deviceConfiguration, T deviceData) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.onChange.GatewayAppDeviceConfigEnum.T:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.onChange.GatewayAppDeviceConfigEnum.T:DA-ELSE
        logger.info("sensor listener called for sensor usage 'Illuminance'");
        logger.info("calling business logic for sensor usage 'Illuminance': LightControl");
        this.gatewayLogicExecutorLightControlBusinessLogic.executeIlluminance(deviceConfiguration, deviceData);
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.onChange.GatewayAppDeviceConfigEnum.T:DA-END
    }
    
    /**
     *
     * @param deviceConfiguration  the deviceConfiguration
     * @param th  the th
     */
    @Override
    public void onError(GatewayAppDeviceConfigEnum deviceConfiguration, Throwable th) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.onError.GatewayAppDeviceConfigEnum.Throwable:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.onError.GatewayAppDeviceConfigEnum.Throwable:DA-ELSE
        logger.error("problem while reading sensor data sensor data for sensor/actuator usage 'Illuminance'", th);
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.onError.GatewayAppDeviceConfigEnum.Throwable:DA-END
    }
    
    /**
     * setter for the field gatewayLogicExecutorLightControlBusinessLogic
     *
     *
     *
     * @param gatewayLogicExecutorLightControlBusinessLogic  the gatewayLogicExecutorLightControlBusinessLogic
     */
    public void setGatewayLogicExecutorLightControlBusinessLogic(LightControlBusinessLogicI gatewayLogicExecutorLightControlBusinessLogic) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.setGatewayLogicExecutorLightControlBusinessLogic.LightControlBusinessLogicI:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.setGatewayLogicExecutorLightControlBusinessLogic.LightControlBusinessLogicI:DA-ELSE
        this.gatewayLogicExecutorLightControlBusinessLogic = gatewayLogicExecutorLightControlBusinessLogic;
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.setGatewayLogicExecutorLightControlBusinessLogic.LightControlBusinessLogicI:DA-END
    }
    
    /**
     * unsetter for the field gatewayLogicExecutorLightControlBusinessLogic
     *
     *
     *
     * @param gatewayLogicExecutorLightControlBusinessLogic  the gatewayLogicExecutorLightControlBusinessLogic
     */
    public void unsetGatewayLogicExecutorLightControlBusinessLogic(LightControlBusinessLogicI gatewayLogicExecutorLightControlBusinessLogic) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.unsetGatewayLogicExecutorLightControlBusinessLogic.LightControlBusinessLogicI:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.unsetGatewayLogicExecutorLightControlBusinessLogic.LightControlBusinessLogicI:DA-ELSE
        if (this.gatewayLogicExecutorLightControlBusinessLogic == gatewayLogicExecutorLightControlBusinessLogic) {
            this.gatewayLogicExecutorLightControlBusinessLogic = null;
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.unsetGatewayLogicExecutorLightControlBusinessLogic.LightControlBusinessLogicI:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.IlluminanceListener.additional.elements.in.type:DA-END
} // end of java type