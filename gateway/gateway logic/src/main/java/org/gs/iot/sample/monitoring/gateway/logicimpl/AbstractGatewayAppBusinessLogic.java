package org.gs.iot.sample.monitoring.gateway.logicimpl;

import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppPublishable;
import org.slf4j.Logger;
import java.util.Map;
import org.osgi.util.tracker.ServiceTracker;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceServiceI;
import org.osgi.service.component.ComponentContext;
import org.osgi.framework.BundleContext;


public abstract class AbstractGatewayAppBusinessLogic implements GatewayAppPublishable { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(AbstractGatewayAppBusinessLogic.class);
    
    /**
     * holds configuration properties for the bundle or service
     */
    private Map<String, Object> properties;
    
    private ServiceTracker<?,?> serviceTrackerForPublishable;
    
    private GatewayAppDeviceServiceI deviceService;
    
    /**
     * creates an instance of AbstractGatewayAppBusinessLogic
     */
    public AbstractGatewayAppBusinessLogic() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic:DA-END
    }
    
    
    /**
     *
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void activate(ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-ELSE
        
        
        this.serviceTrackerForPublishable = new ServiceTracker<>(bundleContext, GatewayAppPublishable.class, null);
        this.serviceTrackerForPublishable.open();
        
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-END
    }
    
    /**
     *
     * @param componentContext  the componentContext
     */
    protected void modified(ComponentContext componentContext) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.modified.ComponentContext:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.modified.ComponentContext:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.modified.ComponentContext:DA-END
    }
    
    /**
     *
     * @param reason  the reason
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-END
    }
    
    /**
     *
     * @param resource  the resource
     * @param payload  the payload
     */
    @Override
    public void publish(String resource, byte[] payload) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.publish.String.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.publish.String.byte.ARRAY:DA-ELSE
        
        if (this.serviceTrackerForPublishable != null) {
        	Object[] services = this.serviceTrackerForPublishable.getServices();
        	if (services != null) {
        		for (Object service : services) {
        			GatewayAppPublishable publishable = (GatewayAppPublishable) service;
        			publishable.publish(resource, payload);
        		}
        	}
        }
        
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.publish.String.byte.ARRAY:DA-END
    }
    
    /**
     * setter for the field deviceService
     *
     *
     *
     * @param deviceService  the deviceService
     */
    public void setDeviceService(GatewayAppDeviceServiceI deviceService) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.setDeviceService.GatewayAppDeviceServiceI:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.setDeviceService.GatewayAppDeviceServiceI:DA-ELSE
        this.deviceService = deviceService;
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.setDeviceService.GatewayAppDeviceServiceI:DA-END
    }
    
    /**
     * unsetter for the field deviceService
     *
     *
     *
     * @param deviceService  the deviceService
     */
    public void unsetDeviceService(GatewayAppDeviceServiceI deviceService) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.unsetDeviceService.GatewayAppDeviceServiceI:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.unsetDeviceService.GatewayAppDeviceServiceI:DA-ELSE
        if (this.deviceService == deviceService) {
            this.deviceService = null;
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.unsetDeviceService.GatewayAppDeviceServiceI:DA-END
    }
    
    /**
     * getter for the field deviceService
     *
     *
     *
     * @return
     */
    public GatewayAppDeviceServiceI getDeviceService() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.getDeviceService.GatewayAppDeviceServiceI:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.getDeviceService.GatewayAppDeviceServiceI:DA-ELSE
        return this.deviceService;
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.getDeviceService.GatewayAppDeviceServiceI:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.AbstractGatewayAppBusinessLogic.additional.elements.in.type:DA-END
} // end of java type