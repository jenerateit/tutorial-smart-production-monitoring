package org.gs.iot.sample.monitoring.gateway.logicimpl;

import org.gs.iot.sample.monitoring.gateway.logic.LightControlBusinessLogicI;
import org.slf4j.Logger;
import java.util.Map;
import org.osgi.service.component.ComponentContext;
import org.osgi.framework.BundleContext;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorStatusType;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData;


/**
 * switch light on when too dark, switch light off when very bright
 */
public class LightControlBusinessLogic extends AbstractGatewayAppBusinessLogic implements LightControlBusinessLogicI { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(LightControlBusinessLogic.class);
    
    /**
     * holds configuration properties for the bundle or service
     */
    private Map<String, Object> properties;
    
    /**
     * creates an instance of LightControlBusinessLogic
     */
    public LightControlBusinessLogic() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.LightControlBusinessLogic:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.LightControlBusinessLogic:DA-ELSE
        super();
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.LightControlBusinessLogic:DA-END
    }
    
    
    /**
     *
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void activate(ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.LightControlBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.LightControlBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-ELSE
         super.activate(componentContext, bundleContext, properties);
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.LightControlBusinessLogic.activate.ComponentContext.BundleContext.Map:DA-END
    }
    
    /**
     *
     * @param componentContext  the componentContext
     */
    protected void modified(ComponentContext componentContext) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.LightControlBusinessLogic.modified.ComponentContext:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.LightControlBusinessLogic.modified.ComponentContext:DA-ELSE
         super.modified(componentContext);
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.LightControlBusinessLogic.modified.ComponentContext:DA-END
    }
    
    /**
     *
     * @param reason  the reason
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.LightControlBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.LightControlBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-ELSE
         super.deactivate(reason, componentContext, bundleContext, properties);
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.LightControlBusinessLogic.deactivate.Integer.ComponentContext.BundleContext.Map:DA-END
    }
    
    /**
     *
     * @param deviceConfiguration  the deviceConfiguration
     * @param deviceData  the deviceData
     */
    @Override
    public void executeIlluminance(GatewayAppDeviceConfigEnum deviceConfiguration, DigitalLightTSL2561GroveData deviceData) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.LightControlBusinessLogic.executeIlluminance.GatewayAppDeviceConfigEnum.DigitalLightTSL2561GroveData:DA-START
        DigitalGroveActuatorData lightData = new DigitalGroveActuatorData();
    	if(deviceData.getData().getStatus() > 100) {
        	lightData.setData(new DigitalGroveActuatorStatusType(false));
        }
        else {
        	lightData.setData(new DigitalGroveActuatorStatusType(true));
        }
		getDeviceService().setLight(lightData );
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.LightControlBusinessLogic.executeIlluminance.GatewayAppDeviceConfigEnum.DigitalLightTSL2561GroveData:DA-ELSE
        //System.out.println("called business logic for changed data for sensor usage 'Illuminance', data is: " + deviceData);
        //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.LightControlBusinessLogic.executeIlluminance.GatewayAppDeviceConfigEnum.DigitalLightTSL2561GroveData:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.logicimpl.LightControlBusinessLogic.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logicimpl.LightControlBusinessLogic.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.logicimpl.LightControlBusinessLogic.additional.elements.in.type:DA-END
} // end of java type