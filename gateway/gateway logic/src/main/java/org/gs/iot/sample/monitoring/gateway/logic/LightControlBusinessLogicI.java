package org.gs.iot.sample.monitoring.gateway.logic;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData;


/**
 * switch light on when too dark, switch light off when very bright
 */
public interface LightControlBusinessLogicI { // start of interface

    
    /**
     *
     * @param deviceConfiguration  the deviceConfiguration
     * @param deviceData  the deviceData
     */
    void executeIlluminance(GatewayAppDeviceConfigEnum deviceConfiguration, DigitalLightTSL2561GroveData deviceData);
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.logic.LightControlBusinessLogicI.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.logic.LightControlBusinessLogicI.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.logic.LightControlBusinessLogicI.additional.elements.in.type:DA-END
} // end of java type