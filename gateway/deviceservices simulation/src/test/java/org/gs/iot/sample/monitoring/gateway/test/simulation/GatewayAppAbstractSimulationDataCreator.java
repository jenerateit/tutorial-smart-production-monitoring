package org.gs.iot.sample.monitoring.gateway.test.simulation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;

import org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData;
import org.slf4j.Logger;


public abstract class GatewayAppAbstractSimulationDataCreator { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(GatewayAppAbstractSimulationDataCreator.class);
    
    private final String rangeStart;
    
    private final String rangeEnd;
    
    private final int countOfDataRecords;
    
    private final String directory;
    
    private final String fileName;
    
    /**
     * creates an instance of GatewayAppAbstractSimulationDataCreator
     *
     * @param rangeStart  the rangeStart
     * @param rangeEnd  the rangeEnd
     * @param countOfDataRecords  the countOfDataRecords
     * @param directory  the directory
     * @param fileName  the fileName
     */
    public GatewayAppAbstractSimulationDataCreator(String rangeStart, String rangeEnd, int countOfDataRecords, String directory, String fileName) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.String.String.int.String.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.String.String.int.String.String:DA-ELSE
        this.rangeStart = rangeStart;
        this.rangeEnd = rangeEnd;
        this.countOfDataRecords = countOfDataRecords;
        this.directory = directory;
        this.fileName = fileName;
        //DA-END:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.String.String.int.String.String:DA-END
    }
    
    
    /**
     * getter for the field rangeStart
     *
     *
     *
     * @return
     */
    protected String getRangeStart() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.getRangeStart.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.getRangeStart.String:DA-ELSE
        return this.rangeStart;
        //DA-END:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.getRangeStart.String:DA-END
    }
    
    /**
     * getter for the field rangeEnd
     *
     *
     *
     * @return
     */
    protected String getRangeEnd() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.getRangeEnd.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.getRangeEnd.String:DA-ELSE
        return this.rangeEnd;
        //DA-END:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.getRangeEnd.String:DA-END
    }
    
    /**
     * getter for the field countOfDataRecords
     *
     *
     *
     * @return
     */
    protected int getCountOfDataRecords() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.getCountOfDataRecords.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.getCountOfDataRecords.int:DA-ELSE
        return this.countOfDataRecords;
        //DA-END:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.getCountOfDataRecords.int:DA-END
    }
    
    /**
     * getter for the field directory
     *
     *
     *
     * @return
     */
    protected String getDirectory() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.getDirectory.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.getDirectory.String:DA-ELSE
        return this.directory;
        //DA-END:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.getDirectory.String:DA-END
    }
    
    /**
     * getter for the field fileName
     *
     *
     *
     * @return
     */
    protected String getFileName() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.getFileName.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.getFileName.String:DA-ELSE
        return this.fileName;
        //DA-END:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.getFileName.String:DA-END
    }
    
    /**
     *
     * @param scenarioNumber  the scenarioNumber
     * @return
     */
    public int createScenario(int scenarioNumber) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.createScenario.int.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.createScenario.int.int:DA-ELSE
        String methodName = "createScenario" + scenarioNumber;
        Object result = null;
        try {
        	Method method = this.getClass().getDeclaredMethod(methodName);
        	result = method.invoke(this);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
        	// almost silently ignore this
        	ex.printStackTrace();
            logger.error("could not find method '" + methodName + "', cannot create simulation data", ex);
        }
        
        return result != null && result instanceof Integer ? (Integer)result : -1;
        //DA-END:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.createScenario.int.int:DA-END
    }
    
    /**
     *
     * @param deviceData  the deviceData
     */
    protected void writeSimulationDataFile(Collection<AbstractGatewayAppDeviceData> deviceData) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.writeSimulationDataFile.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.writeSimulationDataFile.Collection:DA-ELSE
        try {
        	File file = new File(this.directory + "/" + this.fileName);
        	if (file.exists()) file.createNewFile();
        	FileWriter fileWriter = new FileWriter(file.getAbsolutePath());
        	BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        	bufferedWriter.write("[");
        	String comma = "";
        	for (AbstractGatewayAppDeviceData deviceDataObject : deviceData) {
        		bufferedWriter.write(comma);
        		bufferedWriter.newLine();
        		bufferedWriter.write(new String(deviceDataObject.getPayload(true)));
        		comma = ",";
        	}
        	bufferedWriter.write("]");
        	bufferedWriter.close();
        } catch (IOException ex) {
        	ex.printStackTrace();
            throw new RuntimeException("problems while writing simulation data to file", ex); 
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.writeSimulationDataFile.Collection:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.test.simulation.GatewayAppAbstractSimulationDataCreator.additional.elements.in.type:DA-END
} // end of java type