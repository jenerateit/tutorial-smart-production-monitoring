package org.gs.iot.sample.monitoring.gateway.test.simulation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData;
import org.slf4j.Logger;


public class MotionSimulationDataCreator extends GatewayAppAbstractSimulationDataCreator { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(MotionSimulationDataCreator.class);
    
    /**
     * creates an instance of MotionSimulationDataCreator
     *
     * @param rangeStart  the rangeStart
     * @param rangeEnd  the rangeEnd
     * @param countOfDataRecords  the countOfDataRecords
     * @param directory  the directory
     * @param fileName  the fileName
     */
    public MotionSimulationDataCreator(String rangeStart, String rangeEnd, int countOfDataRecords, String directory, String fileName) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.String.String.int.String.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.String.String.int.String.String:DA-ELSE
        super(rangeStart, rangeEnd, countOfDataRecords, directory, fileName);
        //DA-END:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.String.String.int.String.String:DA-END
    }
    
    
    /**
     *
     * @return
     */
    public int createScenario1() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.createScenario1.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.createScenario1.int:DA-ELSE
        Collection<AbstractGatewayAppDeviceData> deviceData = new ArrayList<>();
        for (int ii=0; ii<getCountOfDataRecords(); ii++) createObjectForScenario1(deviceData);
        writeSimulationDataFile(deviceData);
        return getCountOfDataRecords();
        //DA-END:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.createScenario1.int:DA-END
    }
    
    /**
     *
     * @return
     */
    public int createScenario2() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.createScenario2.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.createScenario2.int:DA-ELSE
        Collection<AbstractGatewayAppDeviceData> deviceData = new ArrayList<>();
        for (int ii=0; ii<getCountOfDataRecords(); ii++) createObjectForScenario2(deviceData);
        writeSimulationDataFile(deviceData);
        return getCountOfDataRecords();
        //DA-END:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.createScenario2.int:DA-END
    }
    
    /**
     *
     * @return
     */
    public int createScenario3() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.createScenario3.int:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.createScenario3.int:DA-ELSE
        Collection<AbstractGatewayAppDeviceData> deviceData = new ArrayList<>();
        for (int ii=0; ii<getCountOfDataRecords(); ii++) createObjectForScenario3(deviceData);
        writeSimulationDataFile(deviceData);
        return getCountOfDataRecords();
        //DA-END:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.createScenario3.int:DA-END
    }
    
    /**
     *
     * @param deviceData  the deviceData
     */
    public void createObjectForScenario1(Collection<AbstractGatewayAppDeviceData> deviceData) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.createObjectForScenario1.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.createObjectForScenario1.Collection:DA-ELSE
        DigitalGroveSensorData data = new DigitalGroveSensorData();
        data.setTimestamp(new Date());
        deviceData.add(data);
        //DA-END:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.createObjectForScenario1.Collection:DA-END
    }
    
    /**
     *
     * @param deviceData  the deviceData
     */
    public void createObjectForScenario2(Collection<AbstractGatewayAppDeviceData> deviceData) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.createObjectForScenario2.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.createObjectForScenario2.Collection:DA-ELSE
        DigitalGroveSensorData data = new DigitalGroveSensorData();
        data.setTimestamp(new Date());
        deviceData.add(data);
        //DA-END:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.createObjectForScenario2.Collection:DA-END
    }
    
    /**
     *
     * @param deviceData  the deviceData
     */
    public void createObjectForScenario3(Collection<AbstractGatewayAppDeviceData> deviceData) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.createObjectForScenario3.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.createObjectForScenario3.Collection:DA-ELSE
        DigitalGroveSensorData data = new DigitalGroveSensorData();
        data.setTimestamp(new Date());
        deviceData.add(data);
        //DA-END:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.createObjectForScenario3.Collection:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.test.simulation.MotionSimulationDataCreator.additional.elements.in.type:DA-END
} // end of java type