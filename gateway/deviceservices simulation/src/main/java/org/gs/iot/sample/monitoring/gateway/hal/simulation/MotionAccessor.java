package org.gs.iot.sample.monitoring.gateway.hal.simulation;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppPublishable;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppSensorListenerI;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;


public class MotionAccessor extends AbstractGatewayAppAbstractAccessor implements Runnable { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(MotionAccessor.class);
    
    private DigitalGroveSensorData data;
    
    private DigitalGroveSensorData sentData;
    
    private final LinkedList<DigitalGroveSensorData> historyData = new LinkedList<DigitalGroveSensorData>();
    
    private final List<DigitalGroveSensorData> simulationData = new ArrayList<DigitalGroveSensorData>();
    
    /**
     * creates an instance of MotionAccessor
     *
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public MotionAccessor(ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.ServiceTracker.ServiceTracker:DA-ELSE
        super(serviceTrackerForSensorListener, serviceTrackerForPublishable);
        
        String testData = loadTestData(GatewayAppDeviceConfigEnum.MOTION.getName() + "SimulationData.txt");
                
        //ExclusionStrategy exclusionStrategy = new ExclusionStrategy() {
        //	@Override
        //	public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        //		if (fieldAttributes.getName().equals("data") || fieldAttributes.getName().equals("fault")) {
        //			if (fieldAttributes.getDeclaringClass() == DigitalGroveSensorData.class) return true;
        //		}
        //		
        //		return false;
        //	}
        //	
        //	@Override
        //	public boolean shouldSkipClass(Class<?> clazz) {
        //		return false;
        //	}
        //};
        
        Gson gson = new GsonBuilder().setDateFormat(DigitalGroveSensorData.DATE_FORMAT.toPattern()).create();
        Type type = new TypeToken<List<DigitalGroveSensorData>>() {}.getType();
        List<DigitalGroveSensorData> simulationDataReadFromFile = gson.fromJson(testData, type);
        simulationData.addAll(simulationDataReadFromFile);
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.ServiceTracker.ServiceTracker:DA-END
    }
    
    
    /**
     *
     * @return
     */
    @Override
    public void run() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.run.void:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.run.void:DA-ELSE
        
        if (this.data == null || this.simulationData.indexOf(this.data) == this.simulationData.size()-1) {
        	processData( this.simulationData.get(0) );
        } else {
        	processData( this.simulationData.get(this.simulationData.indexOf(this.data)+1) );
        }
        
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.run.void:DA-END
    }
    
    /**
     * getter for the field data
     *
     *
     *
     * @return
     */
    public DigitalGroveSensorData getData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.getData.DigitalGroveSensorData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.getData.DigitalGroveSensorData:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.getData.DigitalGroveSensorData:DA-END
    }
    
    /**
     * getter for the field sentData
     *
     *
     *
     * @return
     */
    public DigitalGroveSensorData getSentData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.getSentData.DigitalGroveSensorData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.getSentData.DigitalGroveSensorData:DA-ELSE
        return this.sentData;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.getSentData.DigitalGroveSensorData:DA-END
    }
    
    /**
     * getter for the field historyData
     *
     *
     *
     * @return
     */
    public LinkedList<DigitalGroveSensorData> getHistoryData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.getHistoryData.LinkedList:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.getHistoryData.LinkedList:DA-ELSE
        return this.historyData;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.getHistoryData.LinkedList:DA-END
    }
    
    /**
     *
     * @param fileName  the fileName
     * @return
     */
    protected String loadTestData(String fileName) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.loadTestData.String.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.loadTestData.String.String:DA-ELSE
        return readFile(fileName);
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.loadTestData.String.String:DA-END
    }
    
    /**
     * compares the fields 'data' and 'sentData' in order to find out, whether it is required to send another message containing actual sensor data
     *
     * @return
     */
    protected boolean isNotificationRequired() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.isNotificationRequired.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.isNotificationRequired.boolean:DA-ELSE
        return (sentData == null || data != null && !sentData.equals(data));
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.isNotificationRequired.boolean:DA-END
    }
    
    /**
     */
    protected void notifyServices() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.notifyServices:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.notifyServices:DA-ELSE
        
        try {
        	
        	if (getServiceTrackerForPublishable() != null) {
            	Object[] services = getServiceTrackerForPublishable().getServices();
            	if (services != null) {
        			for (Object service : services) {
        				GatewayAppPublishable publishable = (GatewayAppPublishable) service;
        				byte[] payload = this.data.getPayload(true);
        				publishable.publish(GatewayAppDeviceConfigEnum.MOTION.getResource(), payload);
        			}
            	}
        	}
        	
        	if (getServiceTrackerForSensorListener() != null) {
            	Object[] services = getServiceTrackerForSensorListener().getServices();
            	if (services != null) {
        			for (Object service : services) {
                        @SuppressWarnings("unchecked")
        				GatewayAppSensorListenerI<DigitalGroveSensorData> sensorListener = (GatewayAppSensorListenerI<DigitalGroveSensorData>) service;
        			    sensorListener.onChange(GatewayAppDeviceConfigEnum.MOTION, this.data);
        			}
            	}
        	}
        
            this.sentData = this.data;  // keep track of what data has been sent to notify other services
        } catch (Throwable th) {
            th.printStackTrace();  // TODO handle this properly, e.g. in case of problems with OSGi services
            logger.error("failed to notify listeners for sensor usage 'Motion'", th);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.notifyServices:DA-END
    }
    
    /**
     *
     * @param data  the data
     */
    protected void processData(DigitalGroveSensorData data) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.processData.DigitalGroveSensorData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.processData.DigitalGroveSensorData:DA-ELSE
        
        if (data == null) return;  // there is no way to handle this situation here
        
        if (this.data != null) {
        	if (historyData.size() == 10) historyData.removeLast();
            historyData.push(this.data);
        }
        
        this.data = data;
        
        if (isNotificationRequired()) {
            notifyServices();
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.processData.DigitalGroveSensorData:DA-END
    }
    
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.activate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.activate:DA-END
    }
    
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.deactivate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.deactivate:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor.additional.elements.in.type:DA-END
} // end of java type