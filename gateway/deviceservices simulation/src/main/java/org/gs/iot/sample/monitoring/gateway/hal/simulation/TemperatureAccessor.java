package org.gs.iot.sample.monitoring.gateway.hal.simulation;

import org.slf4j.Logger;
import org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import org.osgi.util.tracker.ServiceTracker;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppPublishable;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppSensorListenerI;
import java.io.IOException;


public class TemperatureAccessor extends AbstractGatewayAppAbstractAccessor implements Runnable { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(TemperatureAccessor.class);
    
    private TemperatureSensorGroveData data;
    
    private TemperatureSensorGroveData sentData;
    
    private final LinkedList<TemperatureSensorGroveData> historyData = new LinkedList<TemperatureSensorGroveData>();
    
    private final List<TemperatureSensorGroveData> simulationData = new ArrayList<TemperatureSensorGroveData>();
    
    /**
     * creates an instance of TemperatureAccessor
     *
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public TemperatureAccessor(ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.ServiceTracker.ServiceTracker:DA-ELSE
        super(serviceTrackerForSensorListener, serviceTrackerForPublishable);
        
        String testData = loadTestData(GatewayAppDeviceConfigEnum.TEMPERATURE.getName() + "SimulationData.txt");
                
        //ExclusionStrategy exclusionStrategy = new ExclusionStrategy() {
        //	@Override
        //	public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        //		if (fieldAttributes.getName().equals("data") || fieldAttributes.getName().equals("fault")) {
        //			if (fieldAttributes.getDeclaringClass() == TemperatureSensorGroveData.class) return true;
        //		}
        //		
        //		return false;
        //	}
        //	
        //	@Override
        //	public boolean shouldSkipClass(Class<?> clazz) {
        //		return false;
        //	}
        //};
        
        Gson gson = new GsonBuilder().setDateFormat(TemperatureSensorGroveData.DATE_FORMAT.toPattern()).create();
        Type type = new TypeToken<List<TemperatureSensorGroveData>>() {}.getType();
        List<TemperatureSensorGroveData> simulationDataReadFromFile = gson.fromJson(testData, type);
        simulationData.addAll(simulationDataReadFromFile);
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.ServiceTracker.ServiceTracker:DA-END
    }
    
    
    /**
     *
     * @return
     */
    @Override
    public void run() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.run.void:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.run.void:DA-ELSE
        
        if (this.data == null || this.simulationData.indexOf(this.data) == this.simulationData.size()-1) {
        	processData( this.simulationData.get(0) );
        } else {
        	processData( this.simulationData.get(this.simulationData.indexOf(this.data)+1) );
        }
        
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.run.void:DA-END
    }
    
    /**
     * getter for the field data
     *
     *
     *
     * @return
     */
    public TemperatureSensorGroveData getData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.getData.TemperatureSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.getData.TemperatureSensorGroveData:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.getData.TemperatureSensorGroveData:DA-END
    }
    
    /**
     * getter for the field sentData
     *
     *
     *
     * @return
     */
    public TemperatureSensorGroveData getSentData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.getSentData.TemperatureSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.getSentData.TemperatureSensorGroveData:DA-ELSE
        return this.sentData;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.getSentData.TemperatureSensorGroveData:DA-END
    }
    
    /**
     * getter for the field historyData
     *
     *
     *
     * @return
     */
    public LinkedList<TemperatureSensorGroveData> getHistoryData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.getHistoryData.LinkedList:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.getHistoryData.LinkedList:DA-ELSE
        return this.historyData;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.getHistoryData.LinkedList:DA-END
    }
    
    /**
     *
     * @param fileName  the fileName
     * @return
     */
    protected String loadTestData(String fileName) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.loadTestData.String.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.loadTestData.String.String:DA-ELSE
        return readFile(fileName);
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.loadTestData.String.String:DA-END
    }
    
    /**
     * compares the fields 'data' and 'sentData' in order to find out, whether it is required to send another message containing actual sensor data
     *
     * @return
     */
    protected boolean isNotificationRequired() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.isNotificationRequired.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.isNotificationRequired.boolean:DA-ELSE
        return (sentData == null || data != null && !sentData.equals(data));
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.isNotificationRequired.boolean:DA-END
    }
    
    /**
     */
    protected void notifyServices() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.notifyServices:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.notifyServices:DA-ELSE
        
        try {
        	
        	if (getServiceTrackerForPublishable() != null) {
            	Object[] services = getServiceTrackerForPublishable().getServices();
            	if (services != null) {
        			for (Object service : services) {
        				GatewayAppPublishable publishable = (GatewayAppPublishable) service;
        				byte[] payload = this.data.getPayload(true);
        				publishable.publish(GatewayAppDeviceConfigEnum.TEMPERATURE.getResource(), payload);
        			}
            	}
        	}
        	
        	if (getServiceTrackerForSensorListener() != null) {
            	Object[] services = getServiceTrackerForSensorListener().getServices();
            	if (services != null) {
        			for (Object service : services) {
                        @SuppressWarnings("unchecked")
        				GatewayAppSensorListenerI<TemperatureSensorGroveData> sensorListener = (GatewayAppSensorListenerI<TemperatureSensorGroveData>) service;
        			    sensorListener.onChange(GatewayAppDeviceConfigEnum.TEMPERATURE, this.data);
        			}
            	}
        	}
        
            this.sentData = this.data;  // keep track of what data has been sent to notify other services
        } catch (Throwable th) {
            th.printStackTrace();  // TODO handle this properly, e.g. in case of problems with OSGi services
            logger.error("failed to notify listeners for sensor usage 'Temperature'", th);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.notifyServices:DA-END
    }
    
    /**
     *
     * @param data  the data
     */
    protected void processData(TemperatureSensorGroveData data) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.processData.TemperatureSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.processData.TemperatureSensorGroveData:DA-ELSE
        
        if (data == null) return;  // there is no way to handle this situation here
        
        if (this.data != null) {
        	if (historyData.size() == 10) historyData.removeLast();
            historyData.push(this.data);
        }
        
        this.data = data;
        
        if (isNotificationRequired()) {
            notifyServices();
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.processData.TemperatureSensorGroveData:DA-END
    }
    
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.activate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.activate:DA-END
    }
    
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.deactivate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.deactivate:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor.additional.elements.in.type:DA-END
} // end of java type