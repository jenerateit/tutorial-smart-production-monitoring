package org.gs.iot.sample.monitoring.gateway.hal.simulation;

import org.slf4j.Logger;
import org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveData;
import java.util.LinkedList;
import java.util.List;
import java.util.OptionalDouble;
import java.util.ArrayList;
import org.osgi.util.tracker.ServiceTracker;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppPublishable;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppSensorListenerI;
import java.io.IOException;


public class SoundAccessor extends AbstractGatewayAppAbstractAccessor implements Runnable { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(SoundAccessor.class);
    
    private SoundSensorGroveData data;
    
    private SoundSensorGroveData sentData;
    
    private final LinkedList<SoundSensorGroveData> historyData = new LinkedList<SoundSensorGroveData>();
    
    private final List<SoundSensorGroveData> simulationData = new ArrayList<SoundSensorGroveData>();
    
    /**
     * creates an instance of SoundAccessor
     *
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public SoundAccessor(ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.ServiceTracker.ServiceTracker:DA-ELSE
        super(serviceTrackerForSensorListener, serviceTrackerForPublishable);
        
        String testData = loadTestData(GatewayAppDeviceConfigEnum.SOUND.getName() + "SimulationData.txt");
                
        //ExclusionStrategy exclusionStrategy = new ExclusionStrategy() {
        //	@Override
        //	public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        //		if (fieldAttributes.getName().equals("data") || fieldAttributes.getName().equals("fault")) {
        //			if (fieldAttributes.getDeclaringClass() == SoundSensorGroveData.class) return true;
        //		}
        //		
        //		return false;
        //	}
        //	
        //	@Override
        //	public boolean shouldSkipClass(Class<?> clazz) {
        //		return false;
        //	}
        //};
        
        Gson gson = new GsonBuilder().setDateFormat(SoundSensorGroveData.DATE_FORMAT.toPattern()).create();
        Type type = new TypeToken<List<SoundSensorGroveData>>() {}.getType();
        List<SoundSensorGroveData> simulationDataReadFromFile = gson.fromJson(testData, type);
        simulationData.addAll(simulationDataReadFromFile);
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.ServiceTracker.ServiceTracker:DA-END
    }
    
    
    /**
     *
     * @return
     */
    @Override
    public void run() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.run.void:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.run.void:DA-ELSE
        
        if (this.data == null || this.simulationData.indexOf(this.data) == this.simulationData.size()-1) {
        	processData( this.simulationData.get(0) );
        } else {
        	processData( this.simulationData.get(this.simulationData.indexOf(this.data)+1) );
        }
        
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.run.void:DA-END
    }
    
    /**
     * getter for the field data
     *
     *
     *
     * @return
     */
    public SoundSensorGroveData getData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.getData.SoundSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.getData.SoundSensorGroveData:DA-ELSE
        return this.data;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.getData.SoundSensorGroveData:DA-END
    }
    
    /**
     * getter for the field sentData
     *
     *
     *
     * @return
     */
    public SoundSensorGroveData getSentData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.getSentData.SoundSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.getSentData.SoundSensorGroveData:DA-ELSE
        return this.sentData;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.getSentData.SoundSensorGroveData:DA-END
    }
    
    /**
     * getter for the field historyData
     *
     *
     *
     * @return
     */
    public LinkedList<SoundSensorGroveData> getHistoryData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.getHistoryData.LinkedList:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.getHistoryData.LinkedList:DA-ELSE
        return this.historyData;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.getHistoryData.LinkedList:DA-END
    }
    
    /**
     *
     * @param fileName  the fileName
     * @return
     */
    protected String loadTestData(String fileName) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.loadTestData.String.String:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.loadTestData.String.String:DA-ELSE
        return readFile(fileName);
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.loadTestData.String.String:DA-END
    }
    
    /**
     * compares the fields 'data' and 'sentData' in order to find out, whether it is required to send another message containing actual sensor data
     *
     * @return
     */
    protected boolean isNotificationRequired() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.isNotificationRequired.boolean:DA-START
        
    	OptionalDouble avg = historyData.stream().mapToInt(a -> data.getData().getStatus()).average();

    	logger.info(Double.toString(avg.getAsDouble()));
    	
    	return avg.isPresent() && avg.getAsDouble() > 800 ? true : false;
    	
    	
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.isNotificationRequired.boolean:DA-ELSE
        //return (sentData == null || data != null && !sentData.equals(data));
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.isNotificationRequired.boolean:DA-END
    }
    
    /**
     */
    protected void notifyServices() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.notifyServices:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.notifyServices:DA-ELSE
        
        try {
        	
        	if (getServiceTrackerForPublishable() != null) {
            	Object[] services = getServiceTrackerForPublishable().getServices();
            	if (services != null) {
        			for (Object service : services) {
        				GatewayAppPublishable publishable = (GatewayAppPublishable) service;
        				byte[] payload = this.data.getPayload(true);
        				publishable.publish(GatewayAppDeviceConfigEnum.SOUND.getResource(), payload);
        			}
            	}
        	}
        	
        	if (getServiceTrackerForSensorListener() != null) {
            	Object[] services = getServiceTrackerForSensorListener().getServices();
            	if (services != null) {
        			for (Object service : services) {
                        @SuppressWarnings("unchecked")
        				GatewayAppSensorListenerI<SoundSensorGroveData> sensorListener = (GatewayAppSensorListenerI<SoundSensorGroveData>) service;
        			    sensorListener.onChange(GatewayAppDeviceConfigEnum.SOUND, this.data);
        			}
            	}
        	}
        
            this.sentData = this.data;  // keep track of what data has been sent to notify other services
        } catch (Throwable th) {
            th.printStackTrace();  // TODO handle this properly, e.g. in case of problems with OSGi services
            logger.error("failed to notify listeners for sensor usage 'Sound'", th);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.notifyServices:DA-END
    }
    
    /**
     *
     * @param data  the data
     */
    protected void processData(SoundSensorGroveData data) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.processData.SoundSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.processData.SoundSensorGroveData:DA-ELSE
        
        if (data == null) return;  // there is no way to handle this situation here
        
        if (this.data != null) {
        	if (historyData.size() == 10) historyData.removeLast();
            historyData.push(this.data);
        }
        
        this.data = data;
        
        if (isNotificationRequired()) {
            notifyServices();
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.processData.SoundSensorGroveData:DA-END
    }
    
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.activate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.activate:DA-END
    }
    
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.deactivate:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.deactivate:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor.additional.elements.in.type:DA-END
} // end of java type