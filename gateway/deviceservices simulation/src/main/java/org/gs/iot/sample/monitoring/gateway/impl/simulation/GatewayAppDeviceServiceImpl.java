package org.gs.iot.sample.monitoring.gateway.impl.simulation;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData;
import org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveData;
import org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData;
import org.gs.iot.sample.monitoring.gateway.hal.simulation.AlarmAccessor;
import org.gs.iot.sample.monitoring.gateway.hal.simulation.IlluminanceAccessor;
import org.gs.iot.sample.monitoring.gateway.hal.simulation.LightAccessor;
import org.gs.iot.sample.monitoring.gateway.hal.simulation.MotionAccessor;
import org.gs.iot.sample.monitoring.gateway.hal.simulation.SoundAccessor;
import org.gs.iot.sample.monitoring.gateway.hal.simulation.TemperatureAccessor;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceServiceI;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppPublishable;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppSensorListenerI;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.Filter;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.component.ComponentContext;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;


public class GatewayAppDeviceServiceImpl implements GatewayAppDeviceServiceI { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(GatewayAppDeviceServiceImpl.class);
    
    /**
     * holds configuration properties for the bundle or service
     */
    private Map<String, Object> properties;
    
    private MotionAccessor accessorMotion;
    
    private SoundAccessor accessorSound;
    
    private AlarmAccessor accessorAlarm;
    
    private LightAccessor accessorLight;
    
    private IlluminanceAccessor accessorIlluminance;
    
    private TemperatureAccessor accessorTemperature;
    
    private ServiceTracker<?,?> serviceTrackerForPublishable;
    
    private ServiceTracker<?,?> serviceTrackerForSensorListenerForMotion;
    
    private ServiceTracker<?,?> serviceTrackerForSensorListenerForSound;
    
    private ServiceTracker<?,?> serviceTrackerForSensorListenerForAlarm;
    
    private ServiceTracker<?,?> serviceTrackerForSensorListenerForLight;
    
    private ServiceTracker<?,?> serviceTrackerForSensorListenerForIlluminance;
    
    private ServiceTracker<?,?> serviceTrackerForSensorListenerForTemperature;
    
    private ScheduledFuture<?> scheduledFutureForMotion;
    
    private ScheduledFuture<?> scheduledFutureForSound;
    
    private ScheduledFuture<?> scheduledFutureForAlarm;
    
    private ScheduledFuture<?> scheduledFutureForLight;
    
    private ScheduledFuture<?> scheduledFutureForIlluminance;
    
    private ScheduledFuture<?> scheduledFutureForTemperature;
    
    /**
     * creates an instance of GatewayAppDeviceServiceImpl
     */
    public GatewayAppDeviceServiceImpl() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl:DA-END
    }
    
    
    /**
     *
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void activate(ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.activate.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.activate.ComponentContext.BundleContext.Map:DA-ELSE
        this.properties = properties;
        this.serviceTrackerForPublishable = new ServiceTracker<>(bundleContext, GatewayAppPublishable.class, null);
        this.serviceTrackerForPublishable.open();
        
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Motion 
        
        try {
        	String filterString = "(&(" + Constants.OBJECTCLASS + "=" + GatewayAppSensorListenerI.class.getName() + ")"
        			+ "(datatype=" + DigitalGroveSensorData.class.getSimpleName() + ")"
        			+ "(usage=" + GatewayAppDeviceConfigEnum.MOTION.getName() + "))";
        	Filter filter = bundleContext.createFilter(filterString);
            serviceTrackerForSensorListenerForMotion = new ServiceTracker<>(bundleContext, filter, null);
        	serviceTrackerForSensorListenerForMotion.open();
        } catch (InvalidSyntaxException ex) {
        	throw new RuntimeException("invalid filter syntax for service tracker for sensor usage 'Motion' in activation of device service", ex);
        }
        
        this.accessorMotion = new MotionAccessor(serviceTrackerForSensorListenerForMotion, serviceTrackerForPublishable);
        try {
            this.accessorMotion.activate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during activation of sensor/actuator usage 'Motion'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Sound 
        
        try {
        	String filterString = "(&(" + Constants.OBJECTCLASS + "=" + GatewayAppSensorListenerI.class.getName() + ")"
        			+ "(datatype=" + SoundSensorGroveData.class.getSimpleName() + ")"
        			+ "(usage=" + GatewayAppDeviceConfigEnum.SOUND.getName() + "))";
        	Filter filter = bundleContext.createFilter(filterString);
            serviceTrackerForSensorListenerForSound = new ServiceTracker<>(bundleContext, filter, null);
        	serviceTrackerForSensorListenerForSound.open();
        } catch (InvalidSyntaxException ex) {
        	throw new RuntimeException("invalid filter syntax for service tracker for sensor usage 'Sound' in activation of device service", ex);
        }
        
        this.accessorSound = new SoundAccessor(serviceTrackerForSensorListenerForSound, serviceTrackerForPublishable);
        try {
            this.accessorSound.activate();
            this.scheduledFutureForSound = GatewayAppThreadPool.getScheduledThreadPoolExecutor().scheduleWithFixedDelay(this.accessorSound, 100L, 100L, TimeUnit.MILLISECONDS);
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during activation of sensor/actuator usage 'Sound'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Alarm 
        
        try {
        	String filterString = "(&(" + Constants.OBJECTCLASS + "=" + GatewayAppSensorListenerI.class.getName() + ")"
        			+ "(datatype=" + DigitalGroveActuatorData.class.getSimpleName() + ")"
        			+ "(usage=" + GatewayAppDeviceConfigEnum.ALARM.getName() + "))";
        	Filter filter = bundleContext.createFilter(filterString);
            serviceTrackerForSensorListenerForAlarm = new ServiceTracker<>(bundleContext, filter, null);
        	serviceTrackerForSensorListenerForAlarm.open();
        } catch (InvalidSyntaxException ex) {
        	throw new RuntimeException("invalid filter syntax for service tracker for sensor usage 'Alarm' in activation of device service", ex);
        }
        
        this.accessorAlarm = new AlarmAccessor(serviceTrackerForSensorListenerForAlarm, serviceTrackerForPublishable);
        try {
            this.accessorAlarm.activate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during activation of sensor/actuator usage 'Alarm'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Light 
        
        try {
        	String filterString = "(&(" + Constants.OBJECTCLASS + "=" + GatewayAppSensorListenerI.class.getName() + ")"
        			+ "(datatype=" + DigitalGroveActuatorData.class.getSimpleName() + ")"
        			+ "(usage=" + GatewayAppDeviceConfigEnum.LIGHT.getName() + "))";
        	Filter filter = bundleContext.createFilter(filterString);
            serviceTrackerForSensorListenerForLight = new ServiceTracker<>(bundleContext, filter, null);
        	serviceTrackerForSensorListenerForLight.open();
        } catch (InvalidSyntaxException ex) {
        	throw new RuntimeException("invalid filter syntax for service tracker for sensor usage 'Light' in activation of device service", ex);
        }
        
        this.accessorLight = new LightAccessor(serviceTrackerForSensorListenerForLight, serviceTrackerForPublishable);
        try {
            this.accessorLight.activate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during activation of sensor/actuator usage 'Light'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Illuminance 
        
        try {
        	String filterString = "(&(" + Constants.OBJECTCLASS + "=" + GatewayAppSensorListenerI.class.getName() + ")"
        			+ "(datatype=" + DigitalLightTSL2561GroveData.class.getSimpleName() + ")"
        			+ "(usage=" + GatewayAppDeviceConfigEnum.ILLUMINANCE.getName() + "))";
        	Filter filter = bundleContext.createFilter(filterString);
            serviceTrackerForSensorListenerForIlluminance = new ServiceTracker<>(bundleContext, filter, null);
        	serviceTrackerForSensorListenerForIlluminance.open();
        } catch (InvalidSyntaxException ex) {
        	throw new RuntimeException("invalid filter syntax for service tracker for sensor usage 'Illuminance' in activation of device service", ex);
        }
        
        this.accessorIlluminance = new IlluminanceAccessor(serviceTrackerForSensorListenerForIlluminance, serviceTrackerForPublishable);
        try {
            this.accessorIlluminance.activate();
            this.scheduledFutureForIlluminance = GatewayAppThreadPool.getScheduledThreadPoolExecutor().scheduleWithFixedDelay(this.accessorIlluminance, 5000L, 5000L, TimeUnit.MILLISECONDS);
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during activation of sensor/actuator usage 'Illuminance'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Temperature 
        
        try {
        	String filterString = "(&(" + Constants.OBJECTCLASS + "=" + GatewayAppSensorListenerI.class.getName() + ")"
        			+ "(datatype=" + TemperatureSensorGroveData.class.getSimpleName() + ")"
        			+ "(usage=" + GatewayAppDeviceConfigEnum.TEMPERATURE.getName() + "))";
        	Filter filter = bundleContext.createFilter(filterString);
            serviceTrackerForSensorListenerForTemperature = new ServiceTracker<>(bundleContext, filter, null);
        	serviceTrackerForSensorListenerForTemperature.open();
        } catch (InvalidSyntaxException ex) {
        	throw new RuntimeException("invalid filter syntax for service tracker for sensor usage 'Temperature' in activation of device service", ex);
        }
        
        this.accessorTemperature = new TemperatureAccessor(serviceTrackerForSensorListenerForTemperature, serviceTrackerForPublishable);
        try {
            this.accessorTemperature.activate();
            this.scheduledFutureForTemperature = GatewayAppThreadPool.getScheduledThreadPoolExecutor().scheduleWithFixedDelay(this.accessorTemperature, 5000L, 5000L, TimeUnit.MILLISECONDS);
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during activation of sensor/actuator usage 'Temperature'", ex);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.activate.ComponentContext.BundleContext.Map:DA-END
    }
    
    /**
     *
     * @param componentContext  the componentContext
     */
    protected void modified(ComponentContext componentContext) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.modified.ComponentContext:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.modified.ComponentContext:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.modified.ComponentContext:DA-END
    }
    
    /**
     *
     * @param reason  the reason
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.deactivate.Integer.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.deactivate.Integer.ComponentContext.BundleContext.Map:DA-ELSE
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Motion 
        try {
            if (this.scheduledFutureForMotion != null) this.scheduledFutureForMotion.cancel(true);
        } catch (Throwable th) {
            th.printStackTrace();
            logger.error("problem while cancelling reading sensor data for sensor/actuator usage 'Motion'", th);
        }
        try {
            this.accessorMotion.deactivate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during deactivation of sensor/actuator usage 'Motion'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Sound 
        try {
            if (this.scheduledFutureForSound != null) this.scheduledFutureForSound.cancel(true);
        } catch (Throwable th) {
            th.printStackTrace();
            logger.error("problem while cancelling reading sensor data for sensor/actuator usage 'Sound'", th);
        }
        try {
            this.accessorSound.deactivate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during deactivation of sensor/actuator usage 'Sound'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Alarm 
        try {
            if (this.scheduledFutureForAlarm != null) this.scheduledFutureForAlarm.cancel(true);
        } catch (Throwable th) {
            th.printStackTrace();
            logger.error("problem while cancelling reading sensor data for sensor/actuator usage 'Alarm'", th);
        }
        try {
            this.accessorAlarm.deactivate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during deactivation of sensor/actuator usage 'Alarm'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Light 
        try {
            if (this.scheduledFutureForLight != null) this.scheduledFutureForLight.cancel(true);
        } catch (Throwable th) {
            th.printStackTrace();
            logger.error("problem while cancelling reading sensor data for sensor/actuator usage 'Light'", th);
        }
        try {
            this.accessorLight.deactivate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during deactivation of sensor/actuator usage 'Light'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Illuminance 
        try {
            if (this.scheduledFutureForIlluminance != null) this.scheduledFutureForIlluminance.cancel(true);
        } catch (Throwable th) {
            th.printStackTrace();
            logger.error("problem while cancelling reading sensor data for sensor/actuator usage 'Illuminance'", th);
        }
        try {
            this.accessorIlluminance.deactivate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during deactivation of sensor/actuator usage 'Illuminance'", ex);
        }
        // ------------------------------------------------------------------------- 
        // --- handling sensor/hardware usage Temperature 
        try {
            if (this.scheduledFutureForTemperature != null) this.scheduledFutureForTemperature.cancel(true);
        } catch (Throwable th) {
            th.printStackTrace();
            logger.error("problem while cancelling reading sensor data for sensor/actuator usage 'Temperature'", th);
        }
        try {
            this.accessorTemperature.deactivate();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem during deactivation of sensor/actuator usage 'Temperature'", ex);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.deactivate.Integer.ComponentContext.BundleContext.Map:DA-END
    }
    
    /**
     *
     * @return
     */
    public DigitalGroveSensorData getMotion() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getMotion.DigitalGroveSensorData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getMotion.DigitalGroveSensorData:DA-ELSE
        return this.accessorMotion.getData();
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getMotion.DigitalGroveSensorData:DA-END
    }
    
    /**
     *
     * @return
     */
    public DigitalGroveSensorData getMotionLastSentData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getMotionLastSentData.DigitalGroveSensorData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getMotionLastSentData.DigitalGroveSensorData:DA-ELSE
        return this.accessorMotion.getSentData();
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getMotionLastSentData.DigitalGroveSensorData:DA-END
    }
    
    /**
     *
     * @return
     */
    public Collection<DigitalGroveSensorData> getMotionHistoryData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getMotionHistoryData.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getMotionHistoryData.Collection:DA-ELSE
        return Collections.unmodifiableCollection( this.accessorMotion.getHistoryData() );
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getMotionHistoryData.Collection:DA-END
    }
    
    /**
     *
     * @return
     */
    public SoundSensorGroveData getSound() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getSound.SoundSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getSound.SoundSensorGroveData:DA-ELSE
        return this.accessorSound.getData();
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getSound.SoundSensorGroveData:DA-END
    }
    
    /**
     *
     * @return
     */
    public SoundSensorGroveData getSoundLastSentData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getSoundLastSentData.SoundSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getSoundLastSentData.SoundSensorGroveData:DA-ELSE
        return this.accessorSound.getSentData();
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getSoundLastSentData.SoundSensorGroveData:DA-END
    }
    
    /**
     *
     * @return
     */
    public Collection<SoundSensorGroveData> getSoundHistoryData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getSoundHistoryData.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getSoundHistoryData.Collection:DA-ELSE
        return Collections.unmodifiableCollection( this.accessorSound.getHistoryData() );
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getSoundHistoryData.Collection:DA-END
    }
    
    /**
     *
     * @return
     */
    public DigitalGroveActuatorData getAlarm() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getAlarm.DigitalGroveActuatorData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getAlarm.DigitalGroveActuatorData:DA-ELSE
        return this.accessorAlarm.getData();
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getAlarm.DigitalGroveActuatorData:DA-END
    }
    
    /**
     *
     * @return
     */
    public DigitalGroveActuatorData getAlarmLastSentData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getAlarmLastSentData.DigitalGroveActuatorData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getAlarmLastSentData.DigitalGroveActuatorData:DA-ELSE
        return this.accessorAlarm.getSentData();
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getAlarmLastSentData.DigitalGroveActuatorData:DA-END
    }
    
    /**
     *
     * @return
     */
    public Collection<DigitalGroveActuatorData> getAlarmHistoryData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getAlarmHistoryData.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getAlarmHistoryData.Collection:DA-ELSE
        return Collections.unmodifiableCollection( this.accessorAlarm.getHistoryData() );
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getAlarmHistoryData.Collection:DA-END
    }
    
    /**
     *
     * @return
     */
    public DigitalGroveActuatorData getLight() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getLight.DigitalGroveActuatorData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getLight.DigitalGroveActuatorData:DA-ELSE
        return this.accessorLight.getData();
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getLight.DigitalGroveActuatorData:DA-END
    }
    
    /**
     *
     * @return
     */
    public DigitalGroveActuatorData getLightLastSentData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getLightLastSentData.DigitalGroveActuatorData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getLightLastSentData.DigitalGroveActuatorData:DA-ELSE
        return this.accessorLight.getSentData();
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getLightLastSentData.DigitalGroveActuatorData:DA-END
    }
    
    /**
     *
     * @return
     */
    public Collection<DigitalGroveActuatorData> getLightHistoryData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getLightHistoryData.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getLightHistoryData.Collection:DA-ELSE
        return Collections.unmodifiableCollection( this.accessorLight.getHistoryData() );
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getLightHistoryData.Collection:DA-END
    }
    
    /**
     *
     * @return
     */
    public DigitalLightTSL2561GroveData getIlluminance() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getIlluminance.DigitalLightTSL2561GroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getIlluminance.DigitalLightTSL2561GroveData:DA-ELSE
        return this.accessorIlluminance.getData();
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getIlluminance.DigitalLightTSL2561GroveData:DA-END
    }
    
    /**
     *
     * @return
     */
    public DigitalLightTSL2561GroveData getIlluminanceLastSentData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getIlluminanceLastSentData.DigitalLightTSL2561GroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getIlluminanceLastSentData.DigitalLightTSL2561GroveData:DA-ELSE
        return this.accessorIlluminance.getSentData();
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getIlluminanceLastSentData.DigitalLightTSL2561GroveData:DA-END
    }
    
    /**
     *
     * @return
     */
    public Collection<DigitalLightTSL2561GroveData> getIlluminanceHistoryData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getIlluminanceHistoryData.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getIlluminanceHistoryData.Collection:DA-ELSE
        return Collections.unmodifiableCollection( this.accessorIlluminance.getHistoryData() );
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getIlluminanceHistoryData.Collection:DA-END
    }
    
    /**
     *
     * @return
     */
    public TemperatureSensorGroveData getTemperature() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getTemperature.TemperatureSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getTemperature.TemperatureSensorGroveData:DA-ELSE
        return this.accessorTemperature.getData();
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getTemperature.TemperatureSensorGroveData:DA-END
    }
    
    /**
     *
     * @return
     */
    public TemperatureSensorGroveData getTemperatureLastSentData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getTemperatureLastSentData.TemperatureSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getTemperatureLastSentData.TemperatureSensorGroveData:DA-ELSE
        return this.accessorTemperature.getSentData();
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getTemperatureLastSentData.TemperatureSensorGroveData:DA-END
    }
    
    /**
     *
     * @return
     */
    public Collection<TemperatureSensorGroveData> getTemperatureHistoryData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getTemperatureHistoryData.Collection:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getTemperatureHistoryData.Collection:DA-ELSE
        return Collections.unmodifiableCollection( this.accessorTemperature.getHistoryData() );
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.getTemperatureHistoryData.Collection:DA-END
    }
    
    /**
     *
     * @param deviceData  the deviceData
     */
    public void setAlarm(DigitalGroveActuatorData deviceData) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.setAlarm.DigitalGroveActuatorData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.setAlarm.DigitalGroveActuatorData:DA-ELSE
        this.accessorAlarm.setData(deviceData);
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.setAlarm.DigitalGroveActuatorData:DA-END
    }
    
    /**
     *
     * @param deviceData  the deviceData
     */
    public void setLight(DigitalGroveActuatorData deviceData) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.setLight.DigitalGroveActuatorData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.setLight.DigitalGroveActuatorData:DA-ELSE
        this.accessorLight.setData(deviceData);
        //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.setLight.DigitalGroveActuatorData:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.impl.simulation.GatewayAppDeviceServiceImpl.additional.elements.in.type:DA-END
} // end of java type