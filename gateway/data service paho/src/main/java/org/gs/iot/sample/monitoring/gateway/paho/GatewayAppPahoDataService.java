package org.gs.iot.sample.monitoring.gateway.paho;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData;
import org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppActionable;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceServiceI;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppPublishable;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;

import org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveData;


public class GatewayAppPahoDataService implements GatewayAppPublishable, MqttCallback { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(GatewayAppPahoDataService.class);
    
    public static final String PUBLISH_TOPICPREFIX_PROP_NAME = "publish.appTopicPrefix";
    
    public static final String PUBLISH_QOS_PROP_NAME = "publish.qos";
    
    public static final String PUBLISH_RETAIN_PROP_NAME = "publish.retain";
    
    public static final String PUBLISH_PRIORITY_PROP_NAME = "publish.priority";
    
    /**
     * holds configuration properties for the bundle or service
     */
    private Map<String, Object> properties;
    
    private GatewayAppDeviceServiceI deviceService;
    
    /**
     * the Eclipse Paho client to handle MQTT communication with brokers
     */
    private MqttClient pahoClient;
    
    /**
     * connect options for the connect() call
     */
    private final MqttConnectOptions pahoClientConnectOptions = new MqttConnectOptions();
    
    /**
     * counter for the number of connection attempts before waiting to retry the connection
     */
    private int pahoClientConnectAttemptsCounter = 0;
    
    /**
     * a map where the key is a string-typed topic and the value is a service tracker instance
     */
    private final Map<String, ServiceTracker<Object, Object>> serviceTrackersForActionables = new LinkedHashMap<String, ServiceTracker<Object, Object>>();
    
    /**
     * a runnable that has the task to connect to an MQTT broker
     */
    private RunnableFuture<?> mqttConnectorTask;
    
    /**
     * creates an instance of GatewayAppPahoDataService
     */
    public GatewayAppPahoDataService() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService:DA-END
    }
    
    
    /**
     *
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void activate(ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.activate.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.activate.ComponentContext.BundleContext.Map:DA-ELSE
        this.properties = properties;
        
        // there is no business logic defined in the model => no calls to services that implement actionable interface are going to be generated 
        
        // finally, initialize the MQTT client 
        this.initClient();
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.activate.ComponentContext.BundleContext.Map:DA-END
    }
    
    /**
     *
     * @param componentContext  the componentContext
     */
    protected void modified(ComponentContext componentContext) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.modified.ComponentContext:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.modified.ComponentContext:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.modified.ComponentContext:DA-END
    }
    
    /**
     *
     * @param reason  the reason
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.deactivate.Integer.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.deactivate.Integer.ComponentContext.BundleContext.Map:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.deactivate.Integer.ComponentContext.BundleContext.Map:DA-END
    }
    
    /**
     *
     * @param arg0
     * @return
     */
    @Override
    public void connectionLost(Throwable arg0) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.connectionLost.Throwable.void:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.connectionLost.Throwable.void:DA-ELSE
        
        logger.info("connection lost, reason: " + arg0);
        logger.info("restarting connection thread ...");
        Thread thread = new Thread(this.mqttConnectorTask);
        thread.start();
        
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.connectionLost.Throwable.void:DA-END
    }
    
    /**
     *
     * @param arg0
     * @return
     */
    @Override
    public void deliveryComplete(IMqttDeliveryToken arg0) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.deliveryComplete.IMqttDeliveryToken.void:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.deliveryComplete.IMqttDeliveryToken.void:DA-ELSE
        // nothing to be generated here so far 
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.deliveryComplete.IMqttDeliveryToken.void:DA-END
    }
    
    /**
     *
     * @param arg0
     * @param arg1
     * @return
     */
    @Override
    public void messageArrived(String arg0, MqttMessage arg1) throws Exception {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.messageArrived.String.MqttMessage.void:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.messageArrived.String.MqttMessage.void:DA-ELSE
        
        String prefix = (String) properties.get(PUBLISH_TOPICPREFIX_PROP_NAME);
        prefix = prefix == null ? "client-id/app-id/" : prefix;
                
        String topicPart = null;
        if(arg0.startsWith(prefix)) {
        	topicPart = arg0.substring(prefix.length());
        	if (topicPart.startsWith("/")) topicPart = topicPart.substring(1);
        }
        
        String resource = null;
        if (topicPart.startsWith("sensor/")) {
        	resource = topicPart.substring("sensor/".length());
        } else if (topicPart.startsWith("actuator/")) {
        	resource = topicPart.substring("actuator/".length());
        } else if (topicPart.startsWith("logic/")) {
        	resource = topicPart.substring("logic/".length());
        }
        
        if (resource != null) {
        
        	// --- calling business logic through a service tracker
        	ServiceTracker<?,?> serviceTracker = this.serviceTrackersForActionables.get(resource);
        	if (serviceTracker != null) {
        		GatewayAppActionable actionable = (GatewayAppActionable) serviceTracker.getService();
        		actionable.action(arg1.getPayload());
        	}
        	
        	
        	// --- handling requests to get sensor data or to set actuator data
        	GatewayAppDeviceConfigEnum identifiedSensorUsage = null;
        	for (GatewayAppDeviceConfigEnum configEnumEntry : GatewayAppDeviceConfigEnum.values()) {
        		if (configEnumEntry.getResource().equalsIgnoreCase(resource)) {
        			identifiedSensorUsage = configEnumEntry;
        			break;
        		}
        	}
        	
        	if (identifiedSensorUsage != null) {
        		switch (identifiedSensorUsage) {
        	
        	    case MOTION:
        	        if (arg1.getPayload() != null && arg1.getPayload().length > 0) {
        	            @SuppressWarnings("unused")
        	        	DigitalGroveSensorData deviceData = AbstractGatewayAppDeviceData.createInstance(DigitalGroveSensorData.class, arg1.getPayload());
        			    // DigitalGroveSensor is not an actuator => we cannot set a value here
        	        } else {
        				DigitalGroveSensorData deviceData = deviceService.getMotion();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case SOUND:
        	        if (arg1.getPayload() != null && arg1.getPayload().length > 0) {
        	            @SuppressWarnings("unused")
        	        	SoundSensorGroveData deviceData = AbstractGatewayAppDeviceData.createInstance(SoundSensorGroveData.class, arg1.getPayload());
        			    // SoundSensorGrove is not an actuator => we cannot set a value here
        	        } else {
        				SoundSensorGroveData deviceData = deviceService.getSound();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case ALARM:
        	        if (arg1.getPayload() != null && arg1.getPayload().length > 0) {
        	            
        	        	DigitalGroveActuatorData deviceData = AbstractGatewayAppDeviceData.createInstance(DigitalGroveActuatorData.class, arg1.getPayload());
        			    deviceService.setAlarm(deviceData);
        	        } else {
        				DigitalGroveActuatorData deviceData = deviceService.getAlarm();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case LIGHT:
        	        if (arg1.getPayload() != null && arg1.getPayload().length > 0) {
        	            
        	        	DigitalGroveActuatorData deviceData = AbstractGatewayAppDeviceData.createInstance(DigitalGroveActuatorData.class, arg1.getPayload());
        			    deviceService.setLight(deviceData);
        	        } else {
        				DigitalGroveActuatorData deviceData = deviceService.getLight();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case ILLUMINANCE:
        	        if (arg1.getPayload() != null && arg1.getPayload().length > 0) {
        	            @SuppressWarnings("unused")
        	        	DigitalLightTSL2561GroveData deviceData = AbstractGatewayAppDeviceData.createInstance(DigitalLightTSL2561GroveData.class, arg1.getPayload());
        			    // DigitalLightTSL2561Grove is not an actuator => we cannot set a value here
        	        } else {
        				DigitalLightTSL2561GroveData deviceData = deviceService.getIlluminance();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case TEMPERATURE:
        	        if (arg1.getPayload() != null && arg1.getPayload().length > 0) {
        	            @SuppressWarnings("unused")
        	        	TemperatureSensorGroveData deviceData = AbstractGatewayAppDeviceData.createInstance(TemperatureSensorGroveData.class, arg1.getPayload());
        			    // TemperatureSensorGrove is not an actuator => we cannot set a value here
        	        } else {
        				TemperatureSensorGroveData deviceData = deviceService.getTemperature();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        	    default:
        			throw new RuntimeException("unhandled enum entry found: '" + identifiedSensorUsage + "'");
        	    }
        	}
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.messageArrived.String.MqttMessage.void:DA-END
    }
    
    /**
     * setter for the field deviceService
     *
     *
     *
     * @param deviceService  the deviceService
     */
    public void setDeviceService(GatewayAppDeviceServiceI deviceService) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.setDeviceService.GatewayAppDeviceServiceI:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.setDeviceService.GatewayAppDeviceServiceI:DA-ELSE
        this.deviceService = deviceService;
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.setDeviceService.GatewayAppDeviceServiceI:DA-END
    }
    
    /**
     * unsetter for the field deviceService
     *
     *
     *
     * @param deviceService  the deviceService
     */
    public void unsetDeviceService(GatewayAppDeviceServiceI deviceService) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.unsetDeviceService.GatewayAppDeviceServiceI:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.unsetDeviceService.GatewayAppDeviceServiceI:DA-ELSE
        if (this.deviceService == deviceService) {
            this.deviceService = null;
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.unsetDeviceService.GatewayAppDeviceServiceI:DA-END
    }
    
    /**
     *
     * @param resource  the resource
     * @param payload  the payload
     */
    @Override
    public void publish(String resource, byte[] payload) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.publish.String.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.publish.String.byte.ARRAY:DA-ELSE
        
        if (this.pahoClient != null && this.pahoClient.isConnected()) {
            String prefix = (String) properties.get(PUBLISH_TOPICPREFIX_PROP_NAME);
            prefix = prefix == null ? "client-id/app-id/" : prefix;
        	String topic = prefix + (prefix.endsWith("/") ? "" : "/") + resource;
        	try {
        		this.pahoClient.publish(topic, payload, 0, true);
        	} catch (MqttException ex) {
                logger.error("failed to publish data with Paho client for topic '" + topic + "'", ex);
        		ex.printStackTrace();  // TODO handle this in a better way, e.g. use a logging framework
        	}
        } else {
            System.out.println("cannot send message, pahoClient either null (" + this.pahoClient + ") or not connected (connected=" + this.pahoClient.isConnected() + ")! resource:" + resource + ", payload:" + new String(payload));
        }
        
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.publish.String.byte.ARRAY:DA-END
    }
    
    /**
     * called by a separate thread, the method will never return
     */
    private void connect() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.connect:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.connect:DA-ELSE
        
        boolean connected = false;
        while (!connected) {
        	try {
        		System.out.println("trying to connect ...");
        		this.pahoClient.connect(this.pahoClientConnectOptions);
                System.out.println("successfully connected");
        		connected = true;
        		this.pahoClientConnectAttemptsCounter = 0;
        
                // --- once we are successfully connected, we can subscribe
        	    String prefix = ((String) properties.get(PUBLISH_TOPICPREFIX_PROP_NAME));
        	    prefix = prefix == null ? "client-id/app-id/" : prefix;
        	    String subscriptionTopicForActuators = prefix + (prefix.endsWith("/") ? "" : "/") + "actuator/#";
        	    String subscriptionTopicForSensors = prefix + (prefix.endsWith("/") ? "" : "/") + "sensor/#";
        	    String subscriptionTopicForBusinessLogic = prefix + (prefix.endsWith("/") ? "" : "/") + "logic/#";
        		this.pahoClient.subscribe(new String[] {subscriptionTopicForSensors, subscriptionTopicForActuators, subscriptionTopicForBusinessLogic}, new int[] {0, 0, 0});
        	} catch (MqttException ex) {
        		this.pahoClientConnectAttemptsCounter++;
                logger.error("connection failed, number of failures: " + this.pahoClientConnectAttemptsCounter);
        		if (this.pahoClientConnectAttemptsCounter > 0 && this.pahoClientConnectAttemptsCounter % 3 == 0) {
        			// sleep longer and then try again, three times in a row
        			long longSleepMillis = 60000;
                    logger.info("now sleeping longer, for " + longSleepMillis + " milliseconds ...");
        			try { Thread.sleep(longSleepMillis); } catch (InterruptedException e) {}
        		} else {
        		    long shortSleepMillis = 1000;
                    logger.info("now sleeping for " + shortSleepMillis + " milliseconds ...");
        			try { Thread.sleep(shortSleepMillis); } catch (InterruptedException e) {}
        		}
        	}
        }
        logger.info("exiting from connect() ...");
        
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.connect:DA-END
    }
    
    /**
     * initialize the Paho MQTT client
     */
    private void initClient() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.initClient:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.initClient:DA-ELSE
        
        try {
        	MemoryPersistence persistence = new MemoryPersistence();
        	this.pahoClient = new MqttClient("tcp://localhost:1883", "GatewayApp", persistence);
        	this.pahoClient.setCallback(this);
            this.pahoClientConnectOptions.setCleanSession(false);
            this.pahoClientConnectOptions.setKeepAliveInterval(120);
        
        	this.mqttConnectorTask = new RunnableFuture<Object>() {
        
        		@Override
        		public boolean cancel(boolean mayInterruptIfRunning) { return false; }
        
        		@Override
        		public boolean isCancelled() { return false; }
        
        		@Override
        		public boolean isDone() { return false; }
        
        		@Override
        		public Object get() throws InterruptedException, ExecutionException { return null; }
        
        		@Override
        		public Object get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException { return null; }
        
        		@Override
        		public void run() {
        			connect();
        		}
        		
        	};
        	
        	Thread thread = new Thread(mqttConnectorTask);
        	thread.start();
        } catch (MqttException ex) {
        	throw new RuntimeException("problems creating MQTT client, cannot recover from here", ex);
        }
        
        //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.initClient:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.paho.GatewayAppPahoDataService.additional.elements.in.type:DA-END
} // end of java type