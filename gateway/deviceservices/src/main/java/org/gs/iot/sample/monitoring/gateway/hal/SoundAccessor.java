package org.gs.iot.sample.monitoring.gateway.hal;

import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppInterruptCallback;
import org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveData;
import org.slf4j.Logger;
import org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver;
import java.util.LinkedList;
import java.util.OptionalDouble;

import org.osgi.util.tracker.ServiceTracker;
import java.io.IOException;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppPublishable;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppSensorListenerI;


public class SoundAccessor extends AbstractGatewayAppAbstractAccessor implements Runnable, GatewayAppInterruptCallback<SoundSensorGroveData> { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(SoundAccessor.class);
    
    private SoundSensorGroveDeviceDriver driver;
    
    private SoundSensorGroveData data;
    
    private SoundSensorGroveData sentData;
    
    private final LinkedList<SoundSensorGroveData> historyData = new LinkedList<SoundSensorGroveData>();
    
    /**
     * creates an instance of SoundAccessor
     *
     * @param driver  the driver
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public SoundAccessor(SoundSensorGroveDeviceDriver driver, ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.SoundSensorGroveDeviceDriver.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.SoundSensorGroveDeviceDriver.ServiceTracker.ServiceTracker:DA-ELSE
        super(serviceTrackerForSensorListener, serviceTrackerForPublishable);
        this.driver = driver;
        this.driver.setInterruptCallback(this);
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.SoundSensorGroveDeviceDriver.ServiceTracker.ServiceTracker:DA-END
    }
    
    
    /**
     *
     * @return
     */
    @Override
    public void run() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.run.void:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.run.void:DA-ELSE
        
        try {
            SoundSensorGroveData data = this.driver.getData();
            processData(data);
        } catch (Throwable th) {
        	th.printStackTrace();
            logger.error("failed to read data from driver for sensor usage 'Sound'", th);
        }
        
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.run.void:DA-END
    }
    
    /**
     * getter for the field data
     *
     *
     *
     * @return
     */
    public SoundSensorGroveData getData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.getData.SoundSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.getData.SoundSensorGroveData:DA-ELSE
        try {
            logger.info("getting data by driver for sensor usage 'Sound'");
            return this.driver.getData();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem while getting data for sensor/actuator usage 'Sound'", ex);
            return null;
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.getData.SoundSensorGroveData:DA-END
    }
    
    /**
     * getter for the field sentData
     *
     *
     *
     * @return
     */
    public SoundSensorGroveData getSentData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.getSentData.SoundSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.getSentData.SoundSensorGroveData:DA-ELSE
        return this.sentData;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.getSentData.SoundSensorGroveData:DA-END
    }
    
    /**
     * getter for the field historyData
     *
     *
     *
     * @return
     */
    public LinkedList<SoundSensorGroveData> getHistoryData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.getHistoryData.LinkedList:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.getHistoryData.LinkedList:DA-ELSE
        return this.historyData;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.getHistoryData.LinkedList:DA-END
    }
    
    /**
     * compares the fields 'data' and 'sentData' in order to find out, whether it is required to send another message containing actual sensor data
     *
     * @return
     */
    protected boolean isNotificationRequired() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.isNotificationRequired.boolean:DA-START
    	counter--;
    	if(counter == 0) {
    		counter = 10;
        	OptionalDouble avg = historyData.stream().mapToInt(a -> a.getData().getStatus()).average();
        	
        	return avg.isPresent() && avg.getAsDouble() > 10 ? true : false;
    	}
    	else {
    		return false;
    	}
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.isNotificationRequired.boolean:DA-ELSE
        //return (sentData == null || data != null && !sentData.equals(data));
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.isNotificationRequired.boolean:DA-END
    }
    
    /**
     */
    protected void notifyServices() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.notifyServices:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.notifyServices:DA-ELSE
        
        try {
        	
        	if (getServiceTrackerForPublishable() != null) {
            	Object[] services = getServiceTrackerForPublishable().getServices();
            	if (services != null) {
        			for (Object service : services) {
        				GatewayAppPublishable publishable = (GatewayAppPublishable) service;
        				byte[] payload = this.data.getPayload(true);
        				publishable.publish(GatewayAppDeviceConfigEnum.SOUND.getResource(), payload);
        			}
            	}
        	}
        	
        	if (getServiceTrackerForSensorListener() != null) {
            	Object[] services = getServiceTrackerForSensorListener().getServices();
            	if (services != null) {
        			for (Object service : services) {
                        @SuppressWarnings("unchecked")
        				GatewayAppSensorListenerI<SoundSensorGroveData> sensorListener = (GatewayAppSensorListenerI<SoundSensorGroveData>) service;
        			    sensorListener.onChange(GatewayAppDeviceConfigEnum.SOUND, this.data);
        			}
            	}
        	}
        
            this.sentData = this.data;  // keep track of what data has been sent to notify other services
        } catch (Throwable th) {
            th.printStackTrace();  // TODO handle this properly, e.g. in case of problems with OSGi services
            logger.error("failed to notify listeners for sensor usage 'Sound'", th);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.notifyServices:DA-END
    }
    
    /**
     *
     * @param data  the data
     */
    @Override
    public void onInterrupt(SoundSensorGroveData data) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.onInterrupt.SoundSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.onInterrupt.SoundSensorGroveData:DA-ELSE
        processData(data);
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.onInterrupt.SoundSensorGroveData:DA-END
    }
    
    /**
     *
     * @param data  the data
     */
    protected void processData(SoundSensorGroveData data) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.processData.SoundSensorGroveData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.processData.SoundSensorGroveData:DA-ELSE
        
        if (data == null) return;  // there is no way to handle this situation here
        
        if (this.data != null) {
        	if (historyData.size() == 10) historyData.removeLast();
            historyData.push(this.data);
        }
        
        this.data = data;
        
        if (isNotificationRequired()) {
            notifyServices();
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.processData.SoundSensorGroveData:DA-END
    }
    
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.activate:DA-ELSE
        this.driver.activate();
        logger.info("activated driver for sensor usage 'Sound'");
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.activate:DA-END
    }
    
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.deactivate:DA-ELSE
        this.driver.deactivate();
        logger.info("deactivated driver for sensor usage 'Sound'");
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.deactivate:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.additional.elements.in.type:DA-START
    private int counter = 10;
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.hal.SoundAccessor.additional.elements.in.type:DA-END
} // end of java type