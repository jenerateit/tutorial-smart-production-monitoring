package org.gs.iot.sample.monitoring.gateway.hal;

import org.gs.iot.sample.monitoring.gateway.devicedata.AnalogConnection;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalConnection;
import org.gs.iot.sample.monitoring.gateway.devicedata.I2cConnection;
import org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveActuatorDeviceDriver;
import org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver;
import org.gs.iot.sample.monitoring.gateway.driver.DigitalLightTSL2561GroveDeviceDriver;
import org.gs.iot.sample.monitoring.gateway.driver.TemperatureSensorGroveDeviceDriver;
import org.slf4j.Logger;

import org.gs.iot.sample.monitoring.gateway.driver.SoundSensorGroveDeviceDriver;


public class GatewayAppDeviceSetupFactory { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(GatewayAppDeviceSetupFactory.class);
    
    
    /**
     *
     * @return
     */
    public static DigitalGroveSensorDeviceDriver.DeviceSetup getSetupForMotion() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.getSetupForMotion.DeviceSetup:DA-START
    	DigitalConnection connection = new DigitalConnection();
    	connection.setPinNumber(18);
		return new DigitalGroveSensorDeviceDriver.DeviceSetup(connection);
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.getSetupForMotion.DeviceSetup:DA-ELSE
        //return null;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.getSetupForMotion.DeviceSetup:DA-END
    }
    
    /**
     *
     * @return
     */
    public static SoundSensorGroveDeviceDriver.DeviceSetup getSetupForSound() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.getSetupForSound.DeviceSetup:DA-START
    	AnalogConnection connection = new AnalogConnection();
        connection.setChannel(1);
    	return new SoundSensorGroveDeviceDriver.DeviceSetup(connection);
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.getSetupForSound.DeviceSetup:DA-ELSE
        //return null;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.getSetupForSound.DeviceSetup:DA-END
    }
    
    /**
     *
     * @return
     */
    public static DigitalGroveActuatorDeviceDriver.DeviceSetup getSetupForAlarm() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.getSetupForAlarm.DeviceSetup:DA-START
    	DigitalConnection connection = new DigitalConnection();
    	connection.setPinNumber(25);
    	return new DigitalGroveActuatorDeviceDriver.DeviceSetup(connection);
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.getSetupForAlarm.DeviceSetup:DA-ELSE
        //return null;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.getSetupForAlarm.DeviceSetup:DA-END
    }
    
    /**
     *
     * @return
     */
    public static DigitalGroveActuatorDeviceDriver.DeviceSetup getSetupForLight() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.getSetupForLight.DeviceSetup:DA-START
    	DigitalConnection connection = new DigitalConnection();
    	connection.setPinNumber(23);
    	return new DigitalGroveActuatorDeviceDriver.DeviceSetup(connection);
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.getSetupForLight.DeviceSetup:DA-ELSE
        //return null;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.getSetupForLight.DeviceSetup:DA-END
    }
    
    /**
     *
     * @return
     */
    public static DigitalLightTSL2561GroveDeviceDriver.DeviceSetup getSetupForIlluminance() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.getSetupForIlluminance.DeviceSetup:DA-START
    	I2cConnection connection = new I2cConnection();
    	connection.seti2cAddress(0);
    	connection.seti2cBus(0);
		return new DigitalLightTSL2561GroveDeviceDriver.DeviceSetup(connection);
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.getSetupForIlluminance.DeviceSetup:DA-ELSE
        //return null;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.getSetupForIlluminance.DeviceSetup:DA-END
    }
    
    /**
     *
     * @return
     */
    public static TemperatureSensorGroveDeviceDriver.DeviceSetup getSetupForTemperature() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.getSetupForTemperature.DeviceSetup:DA-START
        AnalogConnection connection = new AnalogConnection();
        connection.setChannel(0);
    	return new TemperatureSensorGroveDeviceDriver.DeviceSetup(connection);
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.getSetupForTemperature.DeviceSetup:DA-ELSE
        //return null;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.getSetupForTemperature.DeviceSetup:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.hal.GatewayAppDeviceSetupFactory.additional.elements.in.type:DA-END
} // end of java type