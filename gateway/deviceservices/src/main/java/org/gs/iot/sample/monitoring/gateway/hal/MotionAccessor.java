package org.gs.iot.sample.monitoring.gateway.hal;

import java.io.IOException;
import java.util.LinkedList;

import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData;
import org.gs.iot.sample.monitoring.gateway.driver.DigitalGroveSensorDeviceDriver;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppInterruptCallback;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppPublishable;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppSensorListenerI;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;


public class MotionAccessor extends AbstractGatewayAppAbstractAccessor implements Runnable, GatewayAppInterruptCallback<DigitalGroveSensorData> { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(MotionAccessor.class);
    
    private DigitalGroveSensorDeviceDriver driver;
    
    private DigitalGroveSensorData data;
    
    private DigitalGroveSensorData sentData;
    
    private final LinkedList<DigitalGroveSensorData> historyData = new LinkedList<DigitalGroveSensorData>();
    
    /**
     * creates an instance of MotionAccessor
     *
     * @param driver  the driver
     * @param serviceTrackerForSensorListener  the serviceTrackerForSensorListener
     * @param serviceTrackerForPublishable  the serviceTrackerForPublishable
     */
    public MotionAccessor(DigitalGroveSensorDeviceDriver driver, ServiceTracker<?,?> serviceTrackerForSensorListener, ServiceTracker<?,?> serviceTrackerForPublishable) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.DigitalGroveSensorDeviceDriver.ServiceTracker.ServiceTracker:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.DigitalGroveSensorDeviceDriver.ServiceTracker.ServiceTracker:DA-ELSE
        super(serviceTrackerForSensorListener, serviceTrackerForPublishable);
        this.driver = driver;
        this.driver.setInterruptCallback(this);
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.DigitalGroveSensorDeviceDriver.ServiceTracker.ServiceTracker:DA-END
    }
    
    
    /**
     *
     * @return
     */
    @Override
    public void run() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.run.void:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.run.void:DA-ELSE
        
        try {
            DigitalGroveSensorData data = this.driver.getData();
            processData(data);
        } catch (Throwable th) {
        	th.printStackTrace();
            logger.error("failed to read data from driver for sensor usage 'Motion'", th);
        }
        
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.run.void:DA-END
    }
    
    /**
     * getter for the field data
     *
     *
     *
     * @return
     */
    public DigitalGroveSensorData getData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.getData.DigitalGroveSensorData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.getData.DigitalGroveSensorData:DA-ELSE
        try {
            logger.info("getting data by driver for sensor usage 'Motion'");
            return this.driver.getData();
        } catch (IOException ex) {
            ex.printStackTrace();
            logger.error("problem while getting data for sensor/actuator usage 'Motion'", ex);
            return null;
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.getData.DigitalGroveSensorData:DA-END
    }
    
    /**
     * getter for the field sentData
     *
     *
     *
     * @return
     */
    public DigitalGroveSensorData getSentData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.getSentData.DigitalGroveSensorData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.getSentData.DigitalGroveSensorData:DA-ELSE
        return this.sentData;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.getSentData.DigitalGroveSensorData:DA-END
    }
    
    /**
     * getter for the field historyData
     *
     *
     *
     * @return
     */
    public LinkedList<DigitalGroveSensorData> getHistoryData() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.getHistoryData.LinkedList:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.getHistoryData.LinkedList:DA-ELSE
        return this.historyData;
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.getHistoryData.LinkedList:DA-END
    }
    
    /**
     * compares the fields 'data' and 'sentData' in order to find out, whether it is required to send another message containing actual sensor data
     *
     * @return
     */
    protected boolean isNotificationRequired() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.isNotificationRequired.boolean:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.isNotificationRequired.boolean:DA-ELSE
        return (sentData == null || data != null && !sentData.equals(data));
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.isNotificationRequired.boolean:DA-END
    }
    
    /**
     */
    protected void notifyServices() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.notifyServices:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.notifyServices:DA-ELSE
        
        try {
        	
        	if (getServiceTrackerForPublishable() != null) {
            	Object[] services = getServiceTrackerForPublishable().getServices();
            	if (services != null) {
        			for (Object service : services) {
        				GatewayAppPublishable publishable = (GatewayAppPublishable) service;
        				byte[] payload = this.data.getPayload(true);
        				publishable.publish(GatewayAppDeviceConfigEnum.MOTION.getResource(), payload);
        			}
            	}
        	}
        	
        	if (getServiceTrackerForSensorListener() != null) {
            	Object[] services = getServiceTrackerForSensorListener().getServices();
            	if (services != null) {
        			for (Object service : services) {
                        @SuppressWarnings("unchecked")
        				GatewayAppSensorListenerI<DigitalGroveSensorData> sensorListener = (GatewayAppSensorListenerI<DigitalGroveSensorData>) service;
        			    sensorListener.onChange(GatewayAppDeviceConfigEnum.MOTION, this.data);
        			}
            	}
        	}
        
            this.sentData = this.data;  // keep track of what data has been sent to notify other services
        } catch (Throwable th) {
            th.printStackTrace();  // TODO handle this properly, e.g. in case of problems with OSGi services
            logger.error("failed to notify listeners for sensor usage 'Motion'", th);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.notifyServices:DA-END
    }
    
    /**
     *
     * @param data  the data
     */
    @Override
    public void onInterrupt(DigitalGroveSensorData data) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.onInterrupt.DigitalGroveSensorData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.onInterrupt.DigitalGroveSensorData:DA-ELSE
        processData(data);
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.onInterrupt.DigitalGroveSensorData:DA-END
    }
    
    /**
     *
     * @param data  the data
     */
    protected void processData(DigitalGroveSensorData data) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.processData.DigitalGroveSensorData:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.processData.DigitalGroveSensorData:DA-ELSE
        
        if (data == null) return;  // there is no way to handle this situation here
        
        if (this.data != null) {
        	if (historyData.size() == 10) historyData.removeLast();
            historyData.push(this.data);
        }
        
        this.data = data;
        
        if (isNotificationRequired()) {
            notifyServices();
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.processData.DigitalGroveSensorData:DA-END
    }
    
    /**
     */
    public void activate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.activate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.activate:DA-ELSE
        this.driver.activate();
        logger.info("activated driver for sensor usage 'Motion'");
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.activate:DA-END
    }
    
    /**
     */
    public void deactivate() throws IOException {
        //DA-START:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.deactivate:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.deactivate:DA-ELSE
        this.driver.deactivate();
        logger.info("deactivated driver for sensor usage 'Motion'");
        //DA-END:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.deactivate:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.hal.MotionAccessor.additional.elements.in.type:DA-END
} // end of java type