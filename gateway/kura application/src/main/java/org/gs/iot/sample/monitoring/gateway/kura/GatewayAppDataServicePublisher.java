package org.gs.iot.sample.monitoring.gateway.kura;

import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.kura.configuration.ConfigurableComponent;
import org.eclipse.kura.data.DataService;
import org.eclipse.kura.data.listener.DataServiceListener;
import org.gs.iot.sample.monitoring.gateway.devicedata.AbstractGatewayAppDeviceData;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveActuatorData;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalGroveSensorData;
import org.gs.iot.sample.monitoring.gateway.devicedata.DigitalLightTSL2561GroveData;
import org.gs.iot.sample.monitoring.gateway.devicedata.TemperatureSensorGroveData;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppActionable;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceConfigEnum;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppDeviceServiceI;
import org.gs.iot.sample.monitoring.gateway.interfaces.GatewayAppPublishable;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;

import org.gs.iot.sample.monitoring.gateway.devicedata.SoundSensorGroveData;


public class GatewayAppDataServicePublisher implements GatewayAppPublishable, DataServiceListener, ConfigurableComponent { // start of class

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(GatewayAppDataServicePublisher.class);
    
    public static final String PUBLISH_TOPICPREFIX_PROP_NAME = "publish.appTopicPrefix";
    
    public static final String PUBLISH_QOS_PROP_NAME = "publish.qos";
    
    public static final String PUBLISH_RETAIN_PROP_NAME = "publish.retain";
    
    public static final String PUBLISH_PRIORITY_PROP_NAME = "publish.priority";
    
    /**
     * holds configuration properties for the bundle or service
     */
    private Map<String, Object> properties;
    
    private GatewayAppDeviceServiceI deviceService;
    
    private DataService dataService;
    
    /**
     * a map where the key is a string-typed topic and the value is a service tracker instance
     */
    private final Map<String, ServiceTracker<Object, Object>> serviceTrackersForActionables = new LinkedHashMap<String, ServiceTracker<Object, Object>>();
    
    /**
     * creates an instance of GatewayAppDataServicePublisher
     */
    public GatewayAppDataServicePublisher() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher:DA-ELSE
        //DA-END:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher:DA-END
    }
    
    
    /**
     *
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void activate(ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.activate.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.activate.ComponentContext.BundleContext.Map:DA-ELSE
        this.properties = properties;
        
        
        // --- connect if not yet connected
        try {
            if (!this.dataService.isConnected()) {
                this.dataService.connect();
            }
        
            onConnectionEstablished();  // call this callback from here since it is not getting called when autoconnect=true (is this a Kura bug?)
        } catch (Throwable th) {
        	th.printStackTrace();
            logger.error("failed to connect in component activation", th);
        }
        
        // there is no business logic defined in the model => no calls to services that implement actionable interface are going to be generated 
        //DA-END:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.activate.ComponentContext.BundleContext.Map:DA-END
    }
    
    /**
     *
     * @param componentContext  the componentContext
     */
    protected void modified(ComponentContext componentContext) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.modified.ComponentContext:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.modified.ComponentContext:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.modified.ComponentContext:DA-END
    }
    
    /**
     *
     * @param reason  the reason
     * @param componentContext  the componentContext
     * @param bundleContext  the bundleContext
     * @param properties  the properties
     */
    protected void deactivate(Integer reason, ComponentContext componentContext, BundleContext bundleContext, Map<String, Object> properties) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.deactivate.Integer.ComponentContext.BundleContext.Map:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.deactivate.Integer.ComponentContext.BundleContext.Map:DA-ELSE
        onDisconnecting();
        //DA-END:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.deactivate.Integer.ComponentContext.BundleContext.Map:DA-END
    }
    
    /**
     *
     * @param resource  the resource
     * @param payload  the payload
     */
    @Override
    public void publish(String resource, byte[] payload) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.publish.String.byte.ARRAY:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.publish.String.byte.ARRAY:DA-ELSE
        
        String topic = null;
        try {
        	String prefix = (String) properties.get(PUBLISH_TOPICPREFIX_PROP_NAME);
        	Integer qos = (Integer) properties.get(PUBLISH_QOS_PROP_NAME);
        	Boolean retain = (Boolean) properties.get(PUBLISH_RETAIN_PROP_NAME);
        	Integer priority = (Integer) properties.get(PUBLISH_PRIORITY_PROP_NAME);
        
        	topic = prefix + (prefix.endsWith("/") ? "" : "/") + resource;
        	int effectivePriority = priority != null ? priority : 2;
            if (dataService != null) {
        	    @SuppressWarnings("unused")
        		int messageId = dataService.publish(topic, payload, qos, retain, effectivePriority);
            } else {
                // TODO what do do in this case?
            }
        
        } catch (Throwable th) {
            th.printStackTrace();
            logger.error("failed to publish message for topic '" + topic + "'", th);
            // TODO handle this in a better way
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.publish.String.byte.ARRAY:DA-END
    }
    
    /**
     *
     * @return
     */
    @Override
    public void onConnectionEstablished() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onConnectionEstablished.void:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onConnectionEstablished.void:DA-ELSE
        try {
        	String prefix = ((String) properties.get(PUBLISH_TOPICPREFIX_PROP_NAME));
            String subscriptionTopicForActuators = prefix + (prefix.endsWith("/") ? "" : "/") + "actuator/#";
            String subscriptionTopicForSensors = prefix + (prefix.endsWith("/") ? "" : "/") + "sensor/#";
            String subscriptionTopicForBusinessLogic = prefix + (prefix.endsWith("/") ? "" : "/") + "logic/#";
        	dataService.subscribe(subscriptionTopicForSensors, 0);
            dataService.subscribe(subscriptionTopicForActuators, 0);
            dataService.subscribe(subscriptionTopicForBusinessLogic, 0);
            dataService.addDataServiceListener(this);
        } catch (Throwable th) {
        	th.printStackTrace();
            logger.error("failed to subscribe for topics", th);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onConnectionEstablished.void:DA-END
    }
    
    /**
     *
     * @param arg0
     * @return
     */
    @Override
    public void onConnectionLost(Throwable arg0) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onConnectionLost.Throwable.void:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onConnectionLost.Throwable.void:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onConnectionLost.Throwable.void:DA-END
    }
    
    /**
     *
     * @return
     */
    @Override
    public void onDisconnected() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onDisconnected.void:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onDisconnected.void:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onDisconnected.void:DA-END
    }
    
    /**
     *
     * @return
     */
    @Override
    public void onDisconnecting() {
        //DA-START:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onDisconnecting.void:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onDisconnecting.void:DA-ELSE
        try {
        	String prefix = ((String) properties.get(PUBLISH_TOPICPREFIX_PROP_NAME));
            String subscriptionTopicForActuators = prefix + (prefix.endsWith("/") ? "" : "/") + "actuator/#";
            String subscriptionTopicForSensors = prefix + (prefix.endsWith("/") ? "" : "/") + "sensor/#";
            String subscriptionTopicForBusinessLogic = prefix + (prefix.endsWith("/") ? "" : "/") + "logic/#";
        	dataService.unsubscribe(subscriptionTopicForSensors);
            dataService.unsubscribe(subscriptionTopicForActuators);
            dataService.unsubscribe(subscriptionTopicForBusinessLogic);
            dataService.removeDataServiceListener(this);
        } catch (Throwable th) {
        	th.printStackTrace();
            logger.error("failed to unsubscribe for topics", th);
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onDisconnecting.void:DA-END
    }
    
    /**
     *
     * @param arg0
     * @param arg1
     * @param arg2
     * @param arg3
     * @return
     */
    @Override
    public void onMessageArrived(String arg0, byte[] arg1, int arg2, boolean arg3) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onMessageArrived.String.byte.ARRAY.int.boolean.void:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onMessageArrived.String.byte.ARRAY.int.boolean.void:DA-ELSE
        logger.info("message arrived, topic: " + arg0 + ", message: " + arg1);
        
        String prefix = (String) properties.get(PUBLISH_TOPICPREFIX_PROP_NAME);
                
        String topicPart = null;
        if(arg0.startsWith(prefix)) {
        	topicPart = arg0.substring(prefix.length());
        	if (topicPart.startsWith("/")) topicPart = topicPart.substring(1);
        }
        
        String resource = null;
        if (topicPart.startsWith("sensor/")) {
        	resource = topicPart.substring("sensor/".length());
        } else if (topicPart.startsWith("actuator/")) {
        	resource = topicPart.substring("actuator/".length());
        } else if (topicPart.startsWith("logic/")) {
        	resource = topicPart.substring("logic/".length());
        }
        
        if (resource != null) {
        	
        	// --- calling business logic through a service tracker
        	ServiceTracker<?,?> serviceTracker = this.serviceTrackersForActionables.get(resource);
        	if (serviceTracker != null) {
        		GatewayAppActionable actionable = (GatewayAppActionable) serviceTracker.getService();
        		actionable.action(arg1);
        	}
        	
        	
        	// --- handling requests to get sensor data or to set actuator data
        	GatewayAppDeviceConfigEnum identifiedSensorUsage = null;
        	for (GatewayAppDeviceConfigEnum configEnumEntry : GatewayAppDeviceConfigEnum.values()) {
        		if (configEnumEntry.getResource().equalsIgnoreCase(resource)) {
        			identifiedSensorUsage = configEnumEntry;
        			break;
        		}
        	}
        	
        	if (identifiedSensorUsage != null) {
        		switch (identifiedSensorUsage) {
        	
        	    case MOTION:
        	        if (arg1 != null && arg1.length > 0) {
        	            @SuppressWarnings("unused")
        	        	DigitalGroveSensorData deviceData = AbstractGatewayAppDeviceData.createInstance(DigitalGroveSensorData.class, arg1);
        			    // DigitalGroveSensor is not an actuator => we cannot set a value here
        	        } else {
        				DigitalGroveSensorData deviceData = deviceService.getMotion();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case SOUND:
        	        if (arg1 != null && arg1.length > 0) {
        	            @SuppressWarnings("unused")
        	        	SoundSensorGroveData deviceData = AbstractGatewayAppDeviceData.createInstance(SoundSensorGroveData.class, arg1);
        			    // SoundSensorGrove is not an actuator => we cannot set a value here
        	        } else {
        				SoundSensorGroveData deviceData = deviceService.getSound();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case ALARM:
        	        if (arg1 != null && arg1.length > 0) {
        	            
        	        	DigitalGroveActuatorData deviceData = AbstractGatewayAppDeviceData.createInstance(DigitalGroveActuatorData.class, arg1);
        			    deviceService.setAlarm(deviceData);
        	        } else {
        				DigitalGroveActuatorData deviceData = deviceService.getAlarm();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case LIGHT:
        	        if (arg1 != null && arg1.length > 0) {
        	            
        	        	DigitalGroveActuatorData deviceData = AbstractGatewayAppDeviceData.createInstance(DigitalGroveActuatorData.class, arg1);
        			    deviceService.setLight(deviceData);
        	        } else {
        				DigitalGroveActuatorData deviceData = deviceService.getLight();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case ILLUMINANCE:
        	        if (arg1 != null && arg1.length > 0) {
        	            @SuppressWarnings("unused")
        	        	DigitalLightTSL2561GroveData deviceData = AbstractGatewayAppDeviceData.createInstance(DigitalLightTSL2561GroveData.class, arg1);
        			    // DigitalLightTSL2561Grove is not an actuator => we cannot set a value here
        	        } else {
        				DigitalLightTSL2561GroveData deviceData = deviceService.getIlluminance();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        
        	    case TEMPERATURE:
        	        if (arg1 != null && arg1.length > 0) {
        	            @SuppressWarnings("unused")
        	        	TemperatureSensorGroveData deviceData = AbstractGatewayAppDeviceData.createInstance(TemperatureSensorGroveData.class, arg1);
        			    // TemperatureSensorGrove is not an actuator => we cannot set a value here
        	        } else {
        				TemperatureSensorGroveData deviceData = deviceService.getTemperature();
        				publish(identifiedSensorUsage.getResource(), deviceData.getPayload());
        	        }
        	        break;
        	    default:
        			throw new RuntimeException("unhandled enum entry found: '" + identifiedSensorUsage + "'");
        	    }
        	}
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onMessageArrived.String.byte.ARRAY.int.boolean.void:DA-END
    }
    
    /**
     *
     * @param arg0
     * @param arg1
     * @return
     */
    @Override
    public void onMessageConfirmed(int arg0, String arg1) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onMessageConfirmed.int.String.void:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onMessageConfirmed.int.String.void:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onMessageConfirmed.int.String.void:DA-END
    }
    
    /**
     *
     * @param arg0
     * @param arg1
     * @return
     */
    @Override
    public void onMessagePublished(int arg0, String arg1) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onMessagePublished.int.String.void:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onMessagePublished.int.String.void:DA-ELSE
        return;
        //DA-END:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.onMessagePublished.int.String.void:DA-END
    }
    
    /**
     * setter for the field deviceService
     *
     *
     *
     * @param deviceService  the deviceService
     */
    public void setDeviceService(GatewayAppDeviceServiceI deviceService) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.setDeviceService.GatewayAppDeviceServiceI:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.setDeviceService.GatewayAppDeviceServiceI:DA-ELSE
        this.deviceService = deviceService;
        //DA-END:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.setDeviceService.GatewayAppDeviceServiceI:DA-END
    }
    
    /**
     * unsetter for the field deviceService
     *
     *
     *
     * @param deviceService  the deviceService
     */
    public void unsetDeviceService(GatewayAppDeviceServiceI deviceService) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.unsetDeviceService.GatewayAppDeviceServiceI:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.unsetDeviceService.GatewayAppDeviceServiceI:DA-ELSE
        if (this.deviceService == deviceService) {
            this.deviceService = null;
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.unsetDeviceService.GatewayAppDeviceServiceI:DA-END
    }
    
    /**
     * setter for the field dataService
     *
     *
     *
     * @param dataService  the dataService
     */
    public void setDataService(DataService dataService) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.setDataService.DataService:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.setDataService.DataService:DA-ELSE
        this.dataService = dataService;
        //DA-END:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.setDataService.DataService:DA-END
    }
    
    /**
     * unsetter for the field dataService
     *
     *
     *
     * @param dataService  the dataService
     */
    public void unsetDataService(DataService dataService) {
        //DA-START:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.unsetDataService.DataService:DA-START
        //DA-ELSE:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.unsetDataService.DataService:DA-ELSE
        if (this.dataService == dataService) {
            this.dataService = null;
        }
        //DA-END:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.unsetDataService.DataService:DA-END
    }
    
    //DA-START:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.additional.elements.in.type:DA-START
    //DA-ELSE:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:org.gs.iot.sample.monitoring.gateway.kura.GatewayAppDataServicePublisher.additional.elements.in.type:DA-END
} // end of java type