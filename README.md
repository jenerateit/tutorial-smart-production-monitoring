# README #

[Hardware und Software](https://docs.virtual-developer.com/technical-documentation/smart-production-monitoring/index.html)

Einstellungen in Eclipse
==========================
eclipse.ini:
    -Dfile.encoding=UTF-8

Label Decorations:
    Virtual Developer Decorator
    Maven Version Decorator

Java -> Installed JREs -> Execution Environments:
    JavaSE-1.8

General -> Editors -> Text Editors -> Spelling:
    disable Spell Checking

Virtual Developer -> CC connection data:
    eu-de-001.virtual-developer.com

Virtual Developer -> Generation:
    uncheck both Check-Boxes

General -> Compare/Patch:
    Ignore white space



Virtual Developer Connection Data
===================================
User:
    gsuser@generative-software.com

Password (Secure Key):
    78319404-33b0-4b51-a7ae-ec4f44656590


H2 Data Source Einstellungen in Wildlfy
=========================================
Database Driver Class:
    org.h2.Driver

JDBC URL:
    jdbc:h2:~/iot-dev

JNDI Name:
    java:/H2DS-IOT-DEV

User:
    sa

Password:
    sa


Workspace IOTDEV-Gateway
==========================
Activate Target Definition:
    Kura Target Definition setzen und im Editor reloaden!


Workspace IOTDEV-Server
=========================
H2 Database Installation:
    http://www.h2database.com/html/download.html

Configure Wildfly User "admin":
    add-user.bat in <wildfly-inst>/bin
